package com.bwie.book.mapper;

import com.bwie.book.car.test.Vehicle;
import com.bwie.common.domain.request.Money;
import com.bwie.common.domain.request.RuleList;
import com.bwie.common.domain.request.RulePeople;
import com.bwie.common.domain.request.Users;
import com.bwie.common.domain.response.Picture;
import com.bwie.common.domain.response.Pie;
import com.bwie.common.result.Result;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: ChangSiYue
 * @Description: 常思悦
 * @createTime: 2023- 09- 15- 19:15
 * @param:
 */
@Mapper
public interface UsersMapper {


    List<Users> selectList(@Param("userIds") List<Long> userIds);


    List<Vehicle> findList(RulePeople rulePeople);

    List<RuleList> findList1(RulePeople rulePeople);

    Money interest(RuleList rulePeople);

    void upda(RuleList rulePeople);

    Integer updaLabel(RuleList rulePeople);

    List<Money> show();

    void updaInterest(Money money);

    Money findIntest(@Param("id") Integer id);

    List<Picture> picture();

    List<Pie> pie();

}
