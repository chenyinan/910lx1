package com.bwie.book.car.handler;



import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import com.bwie.book.car.test.RuleEngine;
import com.bwie.book.car.test.TrafficViolationRule;
import com.bwie.book.car.test.Vehicle;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class RuleEngineApp {


    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @XxlJob("time")
    public void shop(){

        ArrayList<Vehicle> vehicles = new ArrayList<>();

        // 创建规则引擎
        RuleEngine ruleEngine = new RuleEngine();

        // 添加交通违章规则
        ruleEngine.addRule(new TrafficViolationRule());

//         创建车辆对象
        Vehicle vehicle = new Vehicle(true,true,"512");
        Vehicle vehicle1 = new Vehicle(false,false,"lxy885656");
        Vehicle vehicle2 = new Vehicle(true,false,"3jlk459645952656");
        Vehicle vehicle3 = new Vehicle(false,true,"941656595656565557");

        // 评估规则并生成处罚决定
        ruleEngine.evaluateRules(vehicle);
        ruleEngine.evaluateRules(vehicle1);
        ruleEngine.evaluateRules(vehicle2);
        ruleEngine.evaluateRules(vehicle3);


        redisTemplate.opsForSet().add("numList", JSON.toJSONString(vehicle));
        System.out.println("all right");

//        for (int i = 0; i <10; i++) {
//            Vehicle vehicle = new Vehicle(true,true,"王司徒");
//            Vehicle vehicle1 = new Vehicle(false,false,"张麻子");
//            Vehicle vehicle2 = new Vehicle(true,false,"荆天明");
//            Vehicle vehicle3 = new Vehicle(false,true,"接口");
//
//            // 评估规则并生成处罚决定
//            ruleEngine.evaluateRules(vehicle);
//            ruleEngine.evaluateRules(vehicle1);
//            ruleEngine.evaluateRules(vehicle2);
//            ruleEngine.evaluateRules(vehicle3);
//
//            if (vehicle1.getType()!=null){
//                vehicles.add(vehicle1);
//            }
//            if (vehicle.getType()!=null){
//                vehicles.add(vehicle);
//            }
//            if (vehicle2.getType()!=null){
//                vehicles.add(vehicle2);
//            }
//            if (vehicle3.getType()!=null){
//                vehicles.add(vehicle3);
//            }
//
//        }
//
//        //创建固定线程池
//        ExecutorService pool = Executors.newFixedThreadPool(100);
//
//        final int totalPageNo = 50; //分50批次
//
//        final int pageSize = 20000; //每页大小是2万条
//        //共10w条数据，每页5000条数据，20个线程
//        final long start = System.currentTimeMillis();
//        //原子类
//        final AtomicInteger atomicInt = new AtomicInteger();
//
//        //集合数据总条数  除以  批次  =  每批插入的数据条数
//        int size = vehicles.size()/totalPageNo;
//        int size1 = vehicles.size()%totalPageNo;
//
//        for (int currentPageNo = 0; currentPageNo < totalPageNo; currentPageNo++) {
//            final int finalCurrentPageNo = currentPageNo;
//            Runnable run = new Runnable() {
//                @Override
//                public void run() {
//                    List<Vehicle> vehicles1 = vehicles.subList(finalCurrentPageNo * size, size * (finalCurrentPageNo + 1));
//
//                    if (vehicles.size()%totalPageNo!=0&&finalCurrentPageNo==totalPageNo-1){
//                        List<Vehicle> vehicles2 = vehicles.subList(size * (finalCurrentPageNo + 1), vehicles.size());
//                        atomicInt.addAndGet(UserBatchHandler.batchSave(vehicles2, Thread.currentThread().getName()));
//                        System.out.println("------------6-------6--6----------");
//                    }
//
//                    atomicInt.addAndGet(UserBatchHandler.batchSave(vehicles1, Thread.currentThread().getName()));
//                    //入库的数据达到一百万条的时候就会有个统计.
//                    if (atomicInt.get() == (totalPageNo * pageSize)) {
//                        //如果有一百万的时候.就会在这里有个结果
//                        System.out.println("同步数据到db，它已经花费 " + ((System.currentTimeMillis() - start) / 1000) + "  秒");
//                    }
//                }
//            };
//            try {
//                Thread.sleep(5);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            pool.execute(run);
//        }
//
//        System.out.println("all Right");
    }




}
