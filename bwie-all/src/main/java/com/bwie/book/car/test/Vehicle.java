package com.bwie.book.car.test;

import lombok.Data;

import java.io.Serializable;

@Data
public class Vehicle implements Serializable {
     private boolean runningRedLight;
     private boolean speeding;
     private String name;
     private String type;

    public Vehicle(boolean runningRedLight, boolean speeding, String name) {
        this.runningRedLight = runningRedLight;
        this.speeding = speeding;
        this.name = name;
    }

//    public boolean isRunningRedLight() {
//         return runningRedLight;
//     }

//     public void setRunningRedLight(boolean runningRedLight) {
//         this.runningRedLight = runningRedLight;
//     }
//
//     public boolean isSpeeding() {
//         return speeding;
//     }
//
//     public void setSpeeding(boolean speeding) {
//         this.speeding = speeding;
//     }
//
//     @Override
//     public String toString() {
//         return "Vehicle{" +
//                 "runningRedLight=" + runningRedLight +
//                 ", speeding=" + speeding +
//                 '}';
//     }
}
