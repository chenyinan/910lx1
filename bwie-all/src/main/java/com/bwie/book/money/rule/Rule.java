package com.bwie.book.money.rule;


import com.bwie.common.domain.request.Money;

public interface Rule {
    boolean evaluate(Money money);//判断方法
    void execute(Money money);//执行方法
}
