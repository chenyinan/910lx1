package com.bwie.book.money.rule1;

import com.bwie.book.money.rule.InterestRule;
import com.bwie.book.money.rule.Rule;
import com.bwie.common.domain.request.Money;
import com.bwie.common.domain.request.RuleList;

import java.util.ArrayList;
import java.util.List;

public class RuleEngine2 {

    private List<Rule1> rules;//一个规则列表（List<Rule>）来存储规则

    public RuleEngine2() {
        rules = new ArrayList<>();
    }

    public void addRule(InterestRule2 rule) {//addRule()方法添加规则
        rules.add(rule);
    }

    public void evaluateRules(RuleList rulePeople) {//evaluateRules()方法对给定的车辆对象进行规则评估和执行


        for (Rule1 rule1 : rules) {
            if (rule1.evaluate(rulePeople)){
                rule1.execute(rulePeople);
            }
        }
    }

}
