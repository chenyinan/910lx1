package com.bwie.book.money.rule1;


import com.bwie.book.mapper.UsersMapper;
import com.bwie.common.domain.request.Money;
import com.bwie.common.domain.request.RuleList;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

public class InterestRule2 implements Rule1 {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Resource
    private UsersMapper usersMapper;

    @Override
    public boolean evaluate(RuleList rulePeople) {

        return rulePeople.getType()!=null;
    }

    @Override
    public void execute(RuleList rulePeople) {
        Money interest=usersMapper.interest(rulePeople);//查找违规对应的处罚措施

        double v = interest.getMoney() + interest.getMoney() * interest.getInterest();
        interest.setInterestMoney(v);
        System.out.println(v);

    }
}
