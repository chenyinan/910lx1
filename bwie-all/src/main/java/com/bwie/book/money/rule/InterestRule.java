package com.bwie.book.money.rule;


import com.bwie.book.car.test.Vehicle;
import com.bwie.book.util.C3p0Util;
import com.bwie.common.domain.request.Money;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class InterestRule implements Rule {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @Override
    public boolean evaluate(Money money) {

        return money.getInterestType()!=null;
    }

    @Override
    public void execute(Money money) {
        double v = money.getMoney() + money.getMoney() * money.getInterest();
        money.setInterestMoney(v);

    }
}
