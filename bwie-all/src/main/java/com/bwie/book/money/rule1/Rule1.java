package com.bwie.book.money.rule1;

import com.bwie.common.domain.request.Money;
import com.bwie.common.domain.request.RuleList;

public interface Rule1 {
    boolean evaluate(RuleList rulePeople);//判断方法
    void execute(RuleList rulePeople);//执行方法
}
