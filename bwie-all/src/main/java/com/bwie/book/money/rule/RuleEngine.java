package com.bwie.book.money.rule;





import com.bwie.book.money.rule.Rule;
import com.bwie.book.car.test.TrafficViolationRule;
import com.bwie.book.car.test.Vehicle;
import com.bwie.common.domain.request.Money;
import com.bwie.common.domain.request.RuleList;

import java.util.ArrayList;
import java.util.List;

//
public class RuleEngine {
    private List<Rule> rules;//一个规则列表（List<Rule>）来存储规则

    public RuleEngine() {
        rules = new ArrayList<>();
    }

    public void addRule(InterestRule rule) {//addRule()方法添加规则
        rules.add(rule);
    }

    public void evaluateRules(Money money) {//evaluateRules()方法对给定的车辆对象进行规则评估和执行


        for (Rule rule1 : rules) {
            if (rule1.evaluate(money)){
                rule1.execute(money);
            }
        }
    }

//    public void evaluateRules1(RuleList ruleList) {//evaluateRules()方法对给定的车辆对象进行规则评估和执行
//
//
//        for (Rule rule1 : rules) {
//            if (rule1.evaluate(ruleList)){
//                rule1.execute(ruleList);
//            }
//        }
//    }


}
