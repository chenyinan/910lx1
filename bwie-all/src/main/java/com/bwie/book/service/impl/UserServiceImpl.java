package com.bwie.book.service.impl;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.common.utils.CollectionUtils;
import com.bwie.book.car.test.Vehicle;
import com.bwie.book.mapper.UsersMapper;
import com.bwie.book.money.rule.InterestRule;
import com.bwie.book.money.rule.Rule;
import com.bwie.book.money.rule.RuleEngine;
import com.bwie.book.service.UserService;
import com.bwie.book.service.UserWrapBatchService;

import com.bwie.common.constants.JwtConstants;
import com.bwie.common.domain.request.*;
import com.bwie.common.domain.response.Picture;
import com.bwie.common.domain.response.Pie;
import com.bwie.common.result.Result;
import com.bwie.common.utils.JwtUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: ChangSiYue
 * @Description: 常思悦
 * @createTime: 2023- 09- 15- 19:14
 * @param:
 */

/**
 * 根据一批用户请求中的 userId，通过一次数据库查询获取对应的用户信息，
 * 并将查询结果以请求对象的 requestId 为键，用户对象为值，存储到一个 Map 中返回。
 */

@Service
@Log4j2
public class UserServiceImpl implements UserService{
    @Resource
    private UsersMapper usersMapper;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Override
    public Map<String, Users> queryUserByIdBatch(List<UserWrapBatchService.Request> userReqs) {

        // 全部参数
        //首先代码通过流操作将 userReqs 中的每个 UserWrapBatchService.Request 对象的 userId 属性提取出来，去重并存储到 userIds 列表中。
       List<Long> userIds = userReqs.stream().map(UserWrapBatchService.Request::getUserId).distinct().collect(Collectors.toList());
      // List<Long> userIds = userReqs.stream().map(UserWrapBatchService.Request::getUserId).collect(Collectors.toList());
       //使用Stream流的map操作将每个userId映射为对应的用户对象。这样可以避免使用传统的for循环，
        // 提高了代码的简洁性和可读性。同时，使用Stream流的map操作还可以利用并行处理的功能，提高查询用户信息的效率。


        log.info("每次请求的参数：[{}]",userIds);

        // 用in语句合并成一条SQL，避免多次请求数据库的IO
        List<Users> users = usersMapper.selectList(userIds);

        //代码通过流操作讲Users列表中的用户对象按照userId进行分组 ，生成一个Map<Long, List<Users>>类型的userGroup
        Map<Long, List<Users>> userGroup = users.stream().collect(Collectors.groupingBy(Users::getId));

        //创建了一个空的HashMap<String, Users>类型的result对象，用于存储最终的结果
        HashMap<String, Users> result = new HashMap<>();

        //代码遍历userReqs列表的每个请求对象，根据请求的对象的userId在userGroup中查找对应的用户列表userList
        userReqs.forEach(val -> {
            List<Users> usersList = userGroup.get(val.getUserId());
            //  List<Users> users1 = userGroup.get(val.getUserId);
            //  userGroup

            // usersList 不为空，将请求对象的requestId作为键，usersList中的第一个用户对象作为值，放入到result里面
            if (!CollectionUtils.isEmpty(usersList)) {
                result.put(val.getRequestId(), usersList.get(0));
            } else {
                // 表示没数据
                //表示没有查询到对应的用户数据，将请求对象的requestId作为键，将值设置为null
                result.put(val.getRequestId(), null);
            }
        });
        //返回result 包含请求对象的requestId和对应用户对象的映射关系的Map
        return result;
    }

    @Override
    public Result<List<Vehicle>> findList(RulePeople rulePeople) {

        List<Vehicle> list=usersMapper.findList(rulePeople);
        return Result.success(list);
    }

    @Override
    public Result<List<RuleList>> findList1(RulePeople rulePeople) {
        List<RuleList> list=usersMapper.findList1(rulePeople);
        return Result.success(list);
    }

    @Override
    public Result money(RuleList rulePeople) {

        Money interest=usersMapper.interest(rulePeople);//查找违规对应的处罚措施

        RuleEngine ruleEngine = new RuleEngine();//建立规则引擎

        ruleEngine.addRule(new InterestRule());//上传规则

        ruleEngine.evaluateRules(interest);//上传参数

        rulePeople.setApproval("00002");

        usersMapper.upda(rulePeople);
        rabbitTemplate.convertAndSend("send",JSON.toJSONString(interest),msg->{
            msg.getMessageProperties().setMessageId(UUID.randomUUID().toString());
            return msg;
        });

        return Result.success(interest.getInterestMoney());//获得罚款数目
    }

    @Override
    public Result label(RuleList rulePeople) {

       Integer label = usersMapper.updaLabel(rulePeople);


       if (rulePeople.getApproval().equals("00001")){
           return Result.success("撤销成功");
       }


       if (rulePeople.getApproval().equals("00003")){
           return Result.success("发送成功，待Boss审核");
       }

        return Result.success("修改成功");

    }


    @Override
    public Result<List<Money>> show() {

        List<Money> list=usersMapper.show();
        return Result.success(list);
    }

    @Override
    public Result updaInterest(Money money) {

        String token = request.getHeader("token");
        String userKey = JwtUtils.getUserKey(token);

        String s = redisTemplate.opsForValue().get(JwtConstants.USER_KEY + userKey);

        UserInfo userInfo = JSON.parseObject(s, UserInfo.class);

        List<Role> roles = userInfo.getRoles();

        for (Role role : roles) {

            System.out.println(role);
            if (role.getId().equals(3)){
                System.out.println();
                usersMapper.updaInterest(money);
                return Result.success("修改成功");
            }
        }


        return Result.error("当前无访问权限，请提升访问权限");

    }

    @Override
    public Result<Money> findIntest(Integer id) {

        Money money=usersMapper.findIntest(id);
        return Result.success(money);
    }

    @Override
    public Result<List<Picture>> picture() {
        List<Picture>  pictures= usersMapper.picture();
        return Result.success(pictures);
    }

    @Override
    public Result<List<Pie>> pie() {

        List<Pie>  pictures= usersMapper.pie();
        return Result.success(pictures);
    }


}
