package com.bwie.book.service;



import com.bwie.book.car.test.Vehicle;
import com.bwie.common.domain.request.Money;
import com.bwie.common.domain.request.RuleList;
import com.bwie.common.domain.request.RulePeople;
import com.bwie.common.domain.request.Users;
import com.bwie.common.domain.response.Picture;
import com.bwie.common.domain.response.Pie;
import com.bwie.common.result.Result;

import java.util.List;
import java.util.Map;

/**
 * @Author: ChangSiYue
 * @Description: 常思悦
 * @createTime: 2023- 09- 15- 18:50
 * @param:
 */
public interface UserService {

    Map<String, Users> queryUserByIdBatch(List<UserWrapBatchService.Request> userReqs);

    Result<List<Vehicle>> findList(RulePeople rulePeople);

    Result<List<RuleList>> findList1(RulePeople rulePeople);

    Result money(RuleList rulePeople);


    Result label(RuleList rulePeople);

    Result<List<Money>> show();

    Result updaInterest(Money money);


    Result<Money> findIntest(Integer id);

    Result<List<Picture>> picture();

    Result<List<Pie>> pie();

}

