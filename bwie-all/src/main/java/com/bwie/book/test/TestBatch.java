package com.bwie.book.test;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
/**
 * @Author: ChangSiYue
 * @Description: 常思悦
 * @createTime: 2023- 09- 15- 19:48
 * @param:高并发查询代码
 */

/**
 * 这段代码的作用是创建200个线程，并发发送HTTP GET请求。
 * 每个线程发送3次请求，参数为随机数。最后打印每个请求的线程名、参数和响应结果
 */
@Log4j2
@Component
public class TestBatch {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    private static int threadCount = 200;

    private final static CountDownLatch COUNT_DOWN_LATCH = new CountDownLatch(threadCount); //为保证200个线程同时并发运行

    //创建一个RestTemplate对象，用于发送Http请求
    private static final RestTemplate restTemplate = new RestTemplate();


    public void list(){
        System.out.println("--------------------------------66666666666---------------------");
    }

//    @Scheduled(cron = "* 0/1 * * * * *")
    @XxlJob("sends")
    public void shop (){


        redisTemplate.opsForValue().set("ald","acnac");

        for (int i = 0; i < threadCount; i++) {//循环开200个线程
            new Thread(new Runnable() {
                @Override
                public void run() {
                    COUNT_DOWN_LATCH.countDown();//每个线程执行完countDown，每次减一
                    try {
                        COUNT_DOWN_LATCH.await(); //此处等待状态，为了让线程同时进行
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                        //模拟每个线程发送3个请求。
                    for (int j = 1; j <= 3; j++) {

                        // 生成一个随机数作为请求的参数，范围是0到3
                        int param = new Random().nextInt(10);
                        //如果参数小于等于0，将其加1，确保参数大于0
                        if (param <=0){
                            param++;
                        }
                        String responseBody = restTemplate.getForObject("http://localhost:9002/merge/" + param, String .class);

                        log.info(Thread.currentThread().getName() + "参数 " + param + " 返回值 " + responseBody);

                    }

                }
            }).start();

        }
    }
}
