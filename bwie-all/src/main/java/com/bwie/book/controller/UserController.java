package com.bwie.book.controller;
import com.alibaba.fastjson.JSON;
import com.bwie.book.car.test.Vehicle;
import com.bwie.book.mapper.UsersMapper;
import com.bwie.book.service.UserService;
import com.bwie.book.service.UserWrapBatchService;
import com.bwie.common.domain.request.Money;
import com.bwie.common.domain.request.RuleList;
import com.bwie.common.domain.request.RulePeople;
import com.bwie.common.domain.request.Users;
import com.bwie.common.domain.response.Picture;
import com.bwie.common.domain.response.Pie;
import com.bwie.common.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


/**
 * @Author: ChangSiYue
 * @Description: 常思悦
 * @createTime: 2023- 09- 15- 18:48
 * @param:
 */
@Api(value = "积分等级管理")
@RequestMapping("/swagger")
@RestController
@Log4j2
public class UserController {

    @Autowired
    private UserWrapBatchService userWrapBatchService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private UsersMapper usersMapper;

    @Autowired
    private HttpServletRequest request;

   @Autowired
   private UserService userService;


    /***
     * 请求合并
     * */
    @ApiOperation("小区消息查询")
    @GetMapping("/merge/{userId}")
    public Result<Users> merge(@PathVariable Long userId) {
        Users users = userWrapBatchService.queryUser(userId);
        return Result.success(users);
    }

    @ApiOperation("小区消息查询")
    @GetMapping("/show")
    public Result userList() {
        ArrayList<Long> longs = new ArrayList<>();
        longs.add(1L);
        List<Users> users = usersMapper.selectList(longs);

        return Result.success(users);
    }

    @PostMapping("/find")
    public Result<List<Vehicle>> find(@RequestBody RulePeople rulePeople){
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(rulePeople));

        Result<List<Vehicle>> result=userService.findList(rulePeople);
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(rulePeople));

        return result;
    }

    @PostMapping("/findList")
    public Result<List<RuleList>> find1(@RequestBody RulePeople rulePeople){
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(rulePeople));
        Result<List<RuleList>> result=userService.findList1(rulePeople);
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), result);
        return result;
    }


    @PostMapping("/money")
    public Result money(@RequestBody RuleList rulePeople){
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(rulePeople));
        Result result=userService.money(rulePeople);
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), result);
        return result;
    }

    @PostMapping("/upda")
    public Result label(@RequestBody RuleList rulePeople){
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(rulePeople));
        Result result=userService.label(rulePeople);
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), result);
        return result;
    }

    @GetMapping("/showMoney")
    public Result<List<Money>> show(){
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());
        Result<List<Money>> result=userService.show();
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), result);
        return result;
    }

    @ApiOperation("规则修改系统")
    @PostMapping("/updaInterest")
    public Result updaInterest(@RequestBody Money money){
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());
        Result  result=userService.updaInterest(money);
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), result);
        return result;
    }


    @PostMapping("/findInterest/{id}")
    public Result<Money> findIntest(@PathVariable Integer id){
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());
        Result<Money>  result=userService.findIntest(id);
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), result);
        return result;
    }

    @GetMapping("/picture")
    public Result<List<Picture>> result(){
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());
        Result<List<Picture>>  result=userService.picture();
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), result);
        return result;
    }
    @GetMapping("/pie")
    public Result<List<Pie>> pie(){
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod());
        Result<List<Pie>>  result=userService.pie();
        log.info("访问名称：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), result);
        return result;
    }



}
