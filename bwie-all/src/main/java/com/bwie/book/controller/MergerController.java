package com.bwie.book.controller;

import com.bwie.book.mapper.UsersMapper;
import com.bwie.book.service.UserService;
import com.bwie.book.service.UserWrapBatchService;
import com.bwie.common.domain.request.Users;
import com.bwie.common.result.Result;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@Log4j2
public class MergerController {


    @Autowired
    private UserWrapBatchService userWrapBatchService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private UsersMapper usersMapper;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private UserService userService;


    /***
     * 请求合并
     * */
    @ApiOperation("小区消息查询")
    @GetMapping("/merge/{userId}")
    public Result<Users> merge(@PathVariable Long userId) {
        Users users = userWrapBatchService.queryUser(userId);
        return Result.success(users);
    }

    @ApiOperation("小区消息查询")
    @GetMapping("/show")
    public Result userList() {
        ArrayList<Long> longs = new ArrayList<>();
        longs.add(1L);
        List<Users> users = usersMapper.selectList(longs);

        return Result.success(users);
    }




}
