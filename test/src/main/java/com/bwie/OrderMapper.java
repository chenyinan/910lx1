package com.bwie;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

@Mapper
public interface OrderMapper {

    @Insert("insert into t_order_(order_id,name) values(#{randomInt},#{name})")
    void insert(@Param("randomInt") int randomInt, String name);


//    void insert(@Param("bigDecimal") BigDecimal bigDecimal, @Param("name") String name);
}
