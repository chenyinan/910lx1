package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class Place {
    private String homeId;
    private String homeName;
    private String homePlace;

}
