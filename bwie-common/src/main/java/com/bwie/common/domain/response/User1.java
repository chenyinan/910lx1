package com.bwie.common.domain.response;

import lombok.Data;

@Data
public class User1 {

    private Integer id;
    private String email;
    private String phone;
    private String name;
}
