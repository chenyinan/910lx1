package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class Money {

    private Integer id;
    private Double money;
    private Double interest;
    private String interestType;
    private Double interestMoney;


}
