package com.bwie.common.domain.request;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserInfo extends User {

     List<Role> roles =new ArrayList<>();
}
