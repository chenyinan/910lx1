package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class User {

    private Integer userId;
    private String userName;
    private String userPhone;
    private String userPass;
}
