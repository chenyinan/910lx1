package com.bwie.common.domain.response;

import lombok.Data;

@Data
public class UserResponse {
    private String code;
    private String phone;
}
