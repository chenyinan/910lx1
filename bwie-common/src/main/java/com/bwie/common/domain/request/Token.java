package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class Token {

    private String token;
    private String time;
}
