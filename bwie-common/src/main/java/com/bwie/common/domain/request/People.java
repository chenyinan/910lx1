package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class People {

    private Integer id;
    private String name;
    private Integer place;
    private String set;
    private Integer old;
    private String people;

}
