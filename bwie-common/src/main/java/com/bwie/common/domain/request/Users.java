package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class Users {
    private Long id;
    private String name;

}
