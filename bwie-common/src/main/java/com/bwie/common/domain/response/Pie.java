package com.bwie.common.domain.response;

import lombok.Data;

@Data
public class Pie {

    private Integer value;
    private String name;
}
