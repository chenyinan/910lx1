package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class Role {

    private Integer id;
    private String rname;
}
