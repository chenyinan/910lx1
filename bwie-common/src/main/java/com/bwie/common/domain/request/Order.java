package com.bwie.common.domain.request;

//import io.swagger.annotations.ApiModel;

import lombok.Data;

import java.io.Serializable;

@Data

public class Order implements Serializable {


    private String number;



    private String code;


    private String phone;

/*    @DateTimeFormat(pattern = "")
    @JsonFormat
    private Date createTime;
    private Date updaTime;*/

    private Integer longs;

    public Order(String number, String code, String phone) {
        this.number = number;
        this.code = code;
        this.phone = phone;
    }

    public Order() {
    }
}
