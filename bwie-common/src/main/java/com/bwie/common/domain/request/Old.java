package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class Old {
    private Integer start;
    private Integer end;
}
