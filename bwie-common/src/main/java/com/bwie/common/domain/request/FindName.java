package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class FindName {
    private String name;
    private String people;
}
