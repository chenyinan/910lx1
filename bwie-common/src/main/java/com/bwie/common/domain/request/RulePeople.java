package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class RulePeople {

    private String type;
    private String name;
}
