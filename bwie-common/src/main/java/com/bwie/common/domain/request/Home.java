package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class Home {
    private Integer homeId;
    private String homeName;
    private String place;
    private String people;
    private Integer many;
    private Double old;
    private Integer boy;
    private Integer girl;
    private Integer olds;
}
