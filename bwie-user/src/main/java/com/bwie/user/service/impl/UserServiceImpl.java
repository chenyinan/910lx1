package com.bwie.user.service.impl;


import com.bwie.common.domain.request.User;
import com.bwie.common.domain.request.UserInfo;
import com.bwie.common.result.Result;
import com.bwie.user.mapper.UserDao;
import com.bwie.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public Result<UserInfo> user(String phone) {

        UserInfo user = userDao.user(phone);
        return Result.success(user);

    }

    @Override
    public Result userAdd(User user) {
        Integer add=userDao.insert(user);
        return Result.success(add>0?"注册成功":"注册失败");
    }

    @Override
    public Result<UserInfo> userInfo(String name) {
        UserInfo userInfo=userDao.userInfo(name);
        return Result.success(userInfo);
    }
}
