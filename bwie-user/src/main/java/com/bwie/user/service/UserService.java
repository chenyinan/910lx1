package com.bwie.user.service;

import com.bwie.common.domain.request.User;
import com.bwie.common.domain.request.UserInfo;

import com.bwie.common.result.Result;

public interface UserService {
    Result<UserInfo> user(String phone);

    Result userAdd(User user);

    Result<UserInfo> userInfo(String name);

}
