package com.bwie.user.mapper;

import com.bwie.common.domain.request.User;
import com.bwie.common.domain.request.UserInfo;

import org.apache.ibatis.annotations.Param;

public interface UserDao {


    UserInfo user(@Param("phone") String phone);

    Integer insert(User user);

    UserInfo userInfo(@Param("name") String name);

}
