package com.bwie.user.mapper;


import com.bwie.common.domain.request.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleDao {

    List<Role> list(@Param("id") Integer id);
}
