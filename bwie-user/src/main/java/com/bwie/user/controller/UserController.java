package com.bwie.user.controller;

import com.alibaba.fastjson.JSON;

import com.bwie.common.domain.request.User;
import com.bwie.common.domain.request.UserInfo;
import com.bwie.common.result.Result;
import com.bwie.user.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @Autowired
    private HttpServletRequest request;

    @PostMapping("/user/{phone}")
    public Result<UserInfo> userResult(@PathVariable String phone){

        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(phone));

        Result<UserInfo> result=userService.user(phone);
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(phone));

        return result;
    }

    @PostMapping("/userAdd")
    public Result userAdd(@RequestBody User user){
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(user));

        Result result=userService.userAdd(user);
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(result));

        return result;
    }

    @PostMapping("/findName/{name}")
    public Result<UserInfo> findName(@PathVariable String name){
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(name));

        Result<UserInfo> result=userService.userInfo(name);
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(result));

        return result;
    }

}
