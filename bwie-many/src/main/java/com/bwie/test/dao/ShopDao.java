package com.bwie.test.dao;

import com.bwie.common.domain.request.People;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShopDao {


    List<People> all(@Param("input") List<Integer> input);

    void find(@Param("requestParam") Integer requestParam);

}
