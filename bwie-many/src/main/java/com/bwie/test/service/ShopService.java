package com.bwie.test.service;

import com.bwie.common.domain.request.People;
import com.bwie.common.result.Result;

import java.util.List;

public interface ShopService {


    Result all(List<Integer> input);

    void find(Integer requestParam);


    Result finds(Integer id);

    Result findId();

    List<People> findAll(List<Integer> input);

}
