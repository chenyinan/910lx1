package com.bwie.test.service.impl;

import com.bwie.common.domain.request.People;
import com.bwie.common.result.Result;
import com.bwie.test.dao.ShopDao;
import com.bwie.test.service.ShopService;
import com.bwie.test.test.BatchCollapser;
import com.bwie.test.test.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class ShpServiceImpl implements ShopService, BatchCollapser.BatchHandler<List<Integer>, Integer> {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Autowired
    private ShopDao shopDao;

    private BatchCollapser<Integer, Integer> batchCollapser;


    @PostConstruct
    private void postConstructorInit() {
        // 当请求数量达到20个，或每过5s合并执行一次请求
        batchCollapser = BatchCollapser.getInstance(ShpServiceImpl.this, 20, 1);
    }

    @Override
    public Result all(List<Integer> input) {


       List<People> list= shopDao.all(input);

        return Result.success("成功");
    }

    @Override
    public void find(Integer requestParam) {
        shopDao.find(requestParam);
    }

    @Override
    public Result finds(Integer id) {

        return null;
    }

    @Override
    public Result findId() {

        Integer requestParam = (int) (Math.random() * 100) + 1;
        batchCollapser.addRequestParam(requestParam);
        long starts = System.currentTimeMillis();
//        shopDao.find(requestParam);
        long end = System.currentTimeMillis();

        long time=starts-end;
        System.out.println("当前请求参数:" + requestParam+"---"+time);
        return Result.success();
    }

    @Override
    public List<People> findAll(List<Integer> input) {
        List<People> list= shopDao.all(input);

        return list;
    }

    @Override
    public Integer handle(List<Integer> input, BatchCollapser.BatchHandlerType handlerType) {
        System.out.println("处理类型:" + handlerType + ",接受到批量请求参数:" + input);
        return input.stream().mapToInt(x -> x).sum();
    }
}
