package com.bwie.mq.feign;

import com.alibaba.fastjson.JSON;
import com.bwie.common.domain.request.Money;
import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.utils.MsgUtil;
import com.bwie.common.utils.TelSmsUtils;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

@Component
@Log4j2
public class RuleRabbit {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @RabbitListener(queuesToDeclare = {@Queue("send")})
    public void rabbit(String msg, Channel channel, Message message) {


        try {

            String messageId = message.getMessageProperties().getMessageId();

            Long sends = redisTemplate.opsForSet().add("sends", messageId);

            if (sends==1){
                Money money = JSON.parseObject(msg, Money.class);
//            TelSmsUtils.sendSms("15106119206","10001",new HashMap<String, String>(){{
//                put("code","你有一条消息");
//            }});

                MsgUtil.sendMsg("15106119206","15106119206");
                System.out.println("------发送成功---");
                channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            }

            //用于告知 RabbitMQ 服务器确认接收到特定消息。在 RabbitMQ 中，消息传输是可靠的，也就是说一旦任务被分配给消费者，就会一直保持处理状态，直到消费者明确地向 RabbitMQ 服务器发送确定消息。
        } catch (IOException e) {
            e.printStackTrace();

            log.error("遗传信息：","send",message,e.getMessage());

            try {
                channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,true);

            }catch (IOException ioException){
                ioException.printStackTrace();
            }

        }

    }
//    @RabbitListener(queuesToDeclare = {@Queue("send")})
//    public void rabbit1(String msg, Channel channel, Message message) {
//
//
//        try {
//
//            String messageId = message.getMessageProperties().getMessageId();
//
//            Long sends = redisTemplate.opsForSet().add("sends", messageId);
//
//            if (sends==1){
//                Money money = JSON.parseObject(msg, Money.class);
////            TelSmsUtils.sendSms("15106119206","10001",new HashMap<String, String>(){{
////                put("code","你有一条消息");
////            }});
//
//                MsgUtil.sendMsg("15106119206","15106119206");
//                System.out.println("------发送成功---");
//                channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
//            }
//
//            //用于告知 RabbitMQ 服务器确认接收到特定消息。在 RabbitMQ 中，消息传输是可靠的，也就是说一旦任务被分配给消费者，就会一直保持处理状态，直到消费者明确地向 RabbitMQ 服务器发送确定消息。
//        } catch (IOException e) {
//            e.printStackTrace();
//
//            log.error("遗传信息：","send",message,e.getMessage());
//
//            try {
//                channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,true);
//
//            }catch (IOException ioException){
//                ioException.printStackTrace();
//            }
//
//        }
//
//    }
}
