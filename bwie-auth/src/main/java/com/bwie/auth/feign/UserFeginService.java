package com.bwie.auth.feign;


import com.bwie.common.domain.request.User;
import com.bwie.common.domain.request.UserInfo;
import com.bwie.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "bwie-user",fallback = UserFehgin.class)
public interface UserFeginService {
    @PostMapping("/user/{phone}")
    public Result<UserInfo> userResult(@PathVariable String phone);

    @PostMapping("/userAdd")
    public Result userAdd(@RequestBody User user);

    @PostMapping("/findName/{name}")
    public Result<UserInfo> findName(@PathVariable String name);
}
