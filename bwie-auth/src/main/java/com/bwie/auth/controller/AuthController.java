package com.bwie.auth.controller;

import com.alibaba.fastjson.JSON;
import com.bwie.auth.service.AuthService;
import com.bwie.common.domain.request.Token;
import com.bwie.common.domain.request.User;

import com.bwie.common.result.Result;
import com.bwie.common.utils.MsgUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@RestController
public class AuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private HttpServletRequest request;

    @PostMapping("/login")
    public Result<Token> login(@RequestBody User user){
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(user));

        Result result=authService.login(user);
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(result));

        MsgUtil.sendMsg("15106119206","csd");
        return result;
    }


    @PostMapping("/insert")
    public Result insert(@RequestBody User user){
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(user));

        Result result=authService.insert(user);
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(result));

        return result;
    }

    @GetMapping("/user/info")
    public Result<User> info(){
        Result<User> result=authService.info();
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(result));

        return result;
    }

    @PostMapping("/user/logout")
    public Result logout(){
        Result result=authService.logout();
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(result));
        return result;
    }


    @PostMapping("/phone")
    public Result phone(){
        Result result=authService.phone();
        log.info("访问路径：查询，访问URI：{}，访问路径：{}，访问参数：{}",request.getRequestURI(),request.getMethod(), JSON.toJSONString(result));
        return result;
    }




}
