package com.bwie.auth.service;

import com.bwie.common.domain.request.User;
import com.bwie.common.result.Result;

public interface AuthService {
    Result login(User user);

    Result insert(User user);

    Result<User> info();

    Result logout();

    Result phone();

}
