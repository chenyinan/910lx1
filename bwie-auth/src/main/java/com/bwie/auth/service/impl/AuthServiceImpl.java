package com.bwie.auth.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.bwie.auth.feign.UserFeginService;
import com.bwie.auth.service.AuthService;
import com.bwie.common.constants.JwtConstants;

import com.bwie.common.domain.request.Token;
import com.bwie.common.domain.request.User;
import com.bwie.common.domain.request.UserInfo;
import com.bwie.common.domain.response.UserResponse;
import com.bwie.common.result.Result;
import com.bwie.common.utils.JwtUtils;
import com.bwie.common.utils.MsgUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private UserFeginService userFeginService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public Result login(User user) {

//        Result<User> result = userFeginService.userResult(user.getUserPhone());
//
//        UserInfo data = result.getData();


        Result<UserInfo> name = userFeginService.findName(user.getUserName());


        UserInfo data = name.getData();

        if (data==null){
            return Result.error("没有该用户，请先注册");
        }

        if (!data.getUserPass().equals(user.getUserPass())){
            return Result.error("密码错误");
        }


        String userKey = UUID.randomUUID().toString().replaceAll("-", "");
        HashMap<String, Object> map = new HashMap<>();
        map.put(JwtConstants.USER_KEY,userKey);
        String token = JwtUtils.createToken(map);

        redisTemplate.opsForValue().set(JwtConstants.USER_KEY+userKey, JSON.toJSONString(data),30, TimeUnit.MINUTES);

        Token token1 = new Token();

        token1.setToken(token);
        token1.setTime("30");

        return Result.success(token1);
    }

    @Override
    public Result insert(User user) {

        Result<UserInfo> result = userFeginService.userResult(user.getUserPhone());

        UserInfo data = result.getData();

        if (data!=null){
            return Result.error("也存在该手机号");
        }

        Result result1 = userFeginService.userAdd(user);

        if (result1.getCode()==500){
            return Result.error(result1.getMsg());
        }


        return Result.success(result1.getData());
    }

    @Override
    public Result<User> info() {

        String token = request.getHeader("token");
        String userKey = JwtUtils.getUserKey(token);

        String s = redisTemplate.opsForValue().get(JwtConstants.USER_KEY + userKey);

        UserInfo userInfo = JSON.parseObject(s, UserInfo.class);


        return Result.success(userInfo);
    }

    @Override
    public Result logout() {
        String token = request.getHeader("token");
        String userKey = JwtUtils.getUserKey(token);
        redisTemplate.delete(JwtConstants.USER_KEY + userKey);
        return Result.error("退出成功");
    }

    @Override
    public Result phone() {

        String numbers = RandomUtil.randomNumbers(4);
        UserResponse userResponse = new UserResponse();
        userResponse.setCode(numbers);
        userResponse.setPhone("15106119206");

        MsgUtil.sendMsg(userResponse.getPhone(),userResponse.getCode());

        rabbitTemplate.convertAndSend("sendsHelp",JSON.toJSONString(userResponse),msg->{
            msg.getMessageProperties().setMessageId(UUID.randomUUID().toString());
            return msg;
        });

        return Result.success("成功");
    }
}
