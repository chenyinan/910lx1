package com.bwie.test.service.impl;

import com.bwie.common.domain.request.FindName;
import com.bwie.common.domain.request.Home;
import com.bwie.common.domain.request.People;
import com.bwie.common.domain.request.Place;
import com.bwie.common.result.Result;
import com.bwie.test.dao.TestDao;
import com.bwie.test.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestDao testDao;

    @Override
    public Result<List<Home>> compart(FindName findName) {
        List<Home> list=testDao.find(findName);
        return Result.success(list);
    }

    @Override
    public Result insert(Place place) {

       Integer integer= testDao.insert(place);
       if (integer>0){
           return Result.success("成功");
       }else {
           return Result.success("失敗");
       }

    }


    @Override
    public Result insertPeoples(People home) {
        Integer integer= testDao.insertPeoples(home);

        if (integer>0){
            return Result.success("成功");
        }else {
            return Result.success("失敗");
        }
    }
}
