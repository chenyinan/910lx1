package com.bwie.test.dao;

import com.bwie.common.domain.request.FindName;
import com.bwie.common.domain.request.Home;
import com.bwie.common.domain.request.People;
import com.bwie.common.domain.request.Place;

import java.util.List;

public interface TestDao {
    List<Home> find(FindName findName);

    Integer insert(Place place);



    Integer insertPeoples(People home);

}
