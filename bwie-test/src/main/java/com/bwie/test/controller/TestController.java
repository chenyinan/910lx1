package com.bwie.test.controller;

import com.alibaba.druid.sql.dialect.postgresql.ast.stmt.PGStartTransactionStatement;
import com.bwie.common.domain.request.FindName;
import com.bwie.common.domain.request.Home;
import com.bwie.common.domain.request.People;
import com.bwie.common.domain.request.Place;
import com.bwie.common.result.Result;
import com.bwie.test.service.TestService;
import com.sun.corba.se.spi.ior.IdentifiableFactoryFinder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.jsqlparser.util.validation.metadata.JdbcDatabaseMetaDataCapability;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/swagger")
@Api(value = "积分等级管理")
public class TestController {

    @Autowired
    private TestService testService;

    @PostMapping("/list")
    @ApiOperation("小区消息查询")
    public Result<List<Home>> list(@RequestBody FindName findName){
        Result<List<Home>> result=  testService.compart(findName);
        return result;
    }

    @PostMapping("/insert")
    @ApiOperation("小区添加")
    public Result insert(@RequestBody Place place){
        Result result=testService.insert(place);
        return result;
    }

    @PostMapping("/insertPeople")
    @ApiOperation("成员添加")
    public Result insertPeople(@RequestBody People home){
        Result result=testService.insertPeoples(home);
        return result;
    }

    @PostMapping("/help")
    @ApiOperation("吃西瓜呢")
    public Result help(){
        return null;
    }


}
