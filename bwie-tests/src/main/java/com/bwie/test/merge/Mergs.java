package com.bwie.test.merge;

import com.bwie.common.domain.request.Users;
import com.bwie.test.util.UserWrapBatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

public class Mergs {

    @Autowired
    private UserWrapBatchService userBatchService;

    private static int threadCount = 30;

    private final static CountDownLatch COUNT_DOWN_LATCH = new CountDownLatch(threadCount); //为保证30个线程同时并发运行

    private static final RestTemplate restTemplate = new RestTemplate();




    public void Tests() {

        for (int i = 0; i < threadCount; i++) {//循环开30个线程
            new Thread(new Runnable() {
                @Override
                public void run() {
                    COUNT_DOWN_LATCH.countDown();//每次减一
                    try {
                        COUNT_DOWN_LATCH.await(); //此处等待状态，为了让30个线程同时进行
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    for (int j = 1; j <= 3; j++) {
                        int param = new Random().nextInt(4);
                        if (param <= 0) {
                            param++;
                        }

                        System.out.println(param);
                        red((long) param);

                    }
                }
            }).start();

        }
    }


     Callable<Users> red(Long id){
         return new Callable<Users>() {
             @Override
             public Users call() throws Exception {
                 System.out.println("---------------"+id);
                 System.out.println(Thread.currentThread().getName() + "参数 " + id + " 返回值 " + userBatchService.queryUser(id));
                 return userBatchService.queryUser(id);
             }
         };
     }
}
