package com.bwie.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.bwie.test.dao")
public class Tests {
    public static void main(String[] args) {
        SpringApplication.run(Tests.class);
    }
}
