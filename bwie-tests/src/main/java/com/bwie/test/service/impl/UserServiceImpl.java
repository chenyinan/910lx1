package com.bwie.test.service.impl;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bwie.common.domain.request.Users;
import com.bwie.common.result.Result;
import com.bwie.test.dao.UsersMapper;
import com.bwie.test.service.UserService;
import com.bwie.test.util.UserWrapBatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserWrapBatchService userBatchService;

    private static int threadCount = 30;

    private final static CountDownLatch COUNT_DOWN_LATCH = new CountDownLatch(threadCount); //为保证30个线程同时并发运行

    @Resource
    private UsersMapper usersMapper;


    @Override
    public Map<String, Users> queryUserByIdBatch(List<UserWrapBatchService.Request> userReqs) {
        // 全部参数
        List<Long> userIds = userReqs.stream().map(UserWrapBatchService.Request::getUserId).collect(Collectors.toList());
        QueryWrapper<Users> queryWrapper = new QueryWrapper<>();
        // 用in语句合并成一条SQL，避免多次请求数据库的IO
        queryWrapper.in("id", userIds);
        List<Users> users = usersMapper.selectList(queryWrapper);
        Map<Long, List<Users>> userGroup = users.stream().collect(Collectors.groupingBy(Users::getId));
        HashMap<String, Users> result = new HashMap<>();
        userReqs.forEach(val -> {
            List<Users> usersList = userGroup.get(val.getUserId());
            if (!CollectionUtils.isEmpty(usersList)) {
                result.put(val.getRequestId(), usersList.get(0));
            } else {
                // 表示没数据
                result.put(val.getRequestId(), null);
            }
        });
        return result;
    }


    @Override
    public Result list() {

        for (int i = 0; i < threadCount; i++) {//循环开30个线程
            new Thread(() -> {
                COUNT_DOWN_LATCH.countDown();//每次减一
                try {
                    COUNT_DOWN_LATCH.await(); //此处等待状态，为了让30个线程同时进行
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                for (int j = 1; j <= 3; j++) {
                    int param = new Random().nextInt(4);
                    if (param <= 0) {
                        param++;
                    }

                    System.out.println(param);
                    red((long) param);


                }
            }).start();

            return Result.success("成功11");
        }
        return Result.success("成功");
    }


    Callable<Users> red(Long id){
        return new Callable<Users>() {
            @Override
            public Users call() throws Exception {
                System.out.println("---------------"+id);
                System.out.println(Thread.currentThread().getName() + "参数 " + id + " 返回值 " + userBatchService.queryUser(id));
                return userBatchService.queryUser(id);
            }
        };
    }

}
