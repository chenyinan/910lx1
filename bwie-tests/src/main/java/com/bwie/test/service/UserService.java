package com.bwie.test.service;

import com.bwie.common.domain.request.Users;
import com.bwie.common.result.Result;
import com.bwie.test.util.UserWrapBatchService;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public interface UserService {
    Map<String, Users> queryUserByIdBatch(List<UserWrapBatchService.Request> userReqs);



    Result list();

}
