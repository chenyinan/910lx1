package com.bwie.test.controller;

import com.bwie.common.domain.request.Users;
import com.bwie.common.result.Result;
import com.bwie.test.service.UserService;
import com.bwie.test.util.UserWrapBatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.Callable;

@RestController
@RequestMapping("/asyncAndMerge")
public class TestController {


    @Autowired
    private UserWrapBatchService userBatchService;

    @Autowired
    private UserService userService;
    /***
     * 请求合并 return new Callable<Users>() {
     *             @Override
     *             public Users call() throws Exception {
     *                 System.out.println("---------------"+userId);
     *                 return userBatchService.queryUser(userId);
     *             }
     *         };
     * */
//    @GetMapping("/merge")
//    public Callable<Users> merge() {
//
////        return    userService.merger();
//
//    }

    @PostMapping("/list")
    private Result list(){
        return userService.list();
    }
}
