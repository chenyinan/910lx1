package com.bwie.test.help;

import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Method;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class Test {

    public static void main(String[] args) throws Exception {


        Class<?> ruleClass = Class.forName("Mergs");
        Object ruleObject = ruleClass.newInstance();
        Method executeMethod = ruleClass.getDeclaredMethod("Tests");
        executeMethod.invoke(ruleObject);
    }
}
