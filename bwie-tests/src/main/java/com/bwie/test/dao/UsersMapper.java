package com.bwie.test.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bwie.common.domain.request.Users;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UsersMapper {
    List<Users> selectList(@Param("queryWrapper") QueryWrapper<Users> queryWrapper);

}
