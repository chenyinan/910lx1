package com.bwie.millil2;


import javax.tools.*;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class StringToExecutableCode {

    public static void main(String[] args) {
        String javaSource = "package com.bwie;\n" +
                "\n" +
                "import com.bwie.domin.Rules;\n" +
                "import org.springframework.beans.BeansException;\n" +
                "import org.springframework.context.ApplicationContext;\n" +
                "import org.springframework.context.ApplicationContextAware;\n" +
                "\n" +
                "import java.util.HashMap;\n" +
                "import java.util.Map;\n" +
                "\n" +
                "public class HelloWorld{\n" +
                "    public  String show(Map<String,Object> parm) {\n" +
                "        String originString = (String) parm.get(\"phone\");\n" +
                "        Integer start = (Integer) parm.get(\"start\");\n" +
                "        Integer end = (Integer) parm.get(\"end\");\n" +
                "        String replace = (String) parm.get(\"replace\");\n" +
                "        if (originString.length() == 11) {\n" +
                "            String prefix = originString.substring(0, start);\n" +
                "            String suffix = originString.substring(end);\n" +
                "\n" +
                "            StringBuffer stringBuffer = new StringBuffer();\n" +
                "            stringBuffer.append(prefix);\n" +
                "\n" +
                "            for (int i = 0; i < end - start; i++) {\n" +
                "                stringBuffer.append(replace);\n" +
                "            }\n" +
                "            stringBuffer.append(suffix);\n" +
                "\n" +
                "            return stringBuffer.toString();\n" +
                "        }\n" +
                "        throw new RuntimeException(\"请传入11位手机号\");\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "}\n";
        JavaFileObject file = new JavaSourceFromString("HelloWorld", javaSource);

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();

        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, Locale.getDefault(), null);

        Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(file);

        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, diagnostics, null, null, compilationUnits);

        Boolean result = task.call();

        if (result != null && result) {
            System.out.println("Compiled successfully!");
            // 调用可执行程序
            try {

                Class<?> clazz = Class.forName("com.bwie.HelloWorld");
               /* Method show = clazz.getDeclaredMethod("show");
                Object o = clazz.getDeclaredConstructor().newInstance();
                show.invoke(o);*/

                SpringContantx.addBean(javaSource,clazz);
                //=====================================================================================
                //为什么这里反射获取不到
                // Class<?> aClass = Class.forName("com.bwie.rule." + className);
                // 创建实例并调用方法
                //Object instance = aClass1.getDeclaredConstructor().newInstance();
                //上面有问题没解决，使用上下文完成
                // 创建一个可配置的应用程序上下文
//                ConfigurableApplicationContext context = new AnnotationConfigApplicationContext();
//
//// 创建一个对象
////                Student student1 = new Student();
////                student1.setName("张三");
////                student1.setAge(10);
//
//// 将对象注册到上下文中
//                context.getBeanFactory().registerSingleton("student", clazz);
//
//// 启动上下文
//                context.refresh();
//
//// 获取注册的对象
//                HelloWorld student = context.getBean("student", HelloWorld.class);
//
//// 使用对象
//                System.out.println(student);
//
//// 关闭上下文
//                context.close();
                Class bean = SpringContantx.getBean(javaSource, Class.class);
                Object instance  = bean.getDeclaredConstructor().newInstance();


                Method execute = bean.getMethod("HelloWorld", Map.class);
                //测试打印用
//        Map<String, Object> params = new HashMap<>();
//        params.put("key", "value");
                HashMap<String, Object> rule = new HashMap<>();
                rule.put("start",3);
                rule.put("end",7);
                rule.put("replace","*");
                rule.put("phone","15803445910");

                Object invoke = execute.invoke(instance, rule);
                System.out.println(invoke.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("Compilation failed!");
            for (Diagnostic<? extends JavaFileObject> diagnostic : diagnostics.getDiagnostics()) {
                System.err.println(diagnostic.getKind() + ": " + diagnostic.getMessage(null));
            }
        }
    }

    static class JavaSourceFromString extends SimpleJavaFileObject {
        final String code;

        JavaSourceFromString(String name, String code) {
            super(URI.create("string:///" + name.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
            this.code = code;
        }

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) {
            return code;
        }
    }

 /*   static class InMemoryJavaFileManager extends ForwardingJavaFileManager<JavaFileManager> {
        private final Map<String, DynamicCompilationExample.InMemoryOutputJavaFile> fileMap = new HashMap<>();

        protected InMemoryJavaFileManager(JavaFileManager fileManager) {
            super(fileManager);
        }

        public ClassLoader getClassLoader() {
            return new ClassLoader() {
                @Override
                protected Class<?> findClass(String name) throws ClassNotFoundException {
                    DynamicCompilationExample.InMemoryOutputJavaFile outputJavaFile = fileMap.get(name);
                    if (outputJavaFile != null) {
                        byte[] bytes = outputJavaFile.getBytes();
                        return defineClass(name, bytes, 0, bytes.length);
                    }
                    return super.findClass(name);
                }
            };
        }

        @Override
        public JavaFileObject getJavaFileForOutput(Location location, String className, JavaFileObject.Kind kind,
                                                   FileObject sibling) {
            DynamicCompilationExample.InMemoryOutputJavaFile outputJavaFile = new DynamicCompilationExample.InMemoryOutputJavaFile(className, kind);
            fileMap.put(className, outputJavaFile);
            return outputJavaFile;
        }
    }
*/
    // 自定义 InMemoryOutputJavaFile，用于存储编译后的class字节码
    static class InMemoryOutputJavaFile extends SimpleJavaFileObject {
        private final ByteArrayOutputStream outputStream;
        private final String className;

        protected InMemoryOutputJavaFile(String className, Kind kind) {
            super(computeURI(className), kind);
            this.className = className;
            this.outputStream = new ByteArrayOutputStream();
        }

        public byte[] getBytes() {
            return outputStream.toByteArray();
        }

        @Override
        public OutputStream openOutputStream() {
            return outputStream;
        }

        private static URI computeURI(String className) {
            return URI.create("string:///" + className.replace('.', '/') + Kind.CLASS.extension);
        }
    }

    // 自定义 InMemorySource，用于存储编译前的源码
    static class InMemorySource extends SimpleJavaFileObject {
        private final String code;

        protected InMemorySource(String className, String code) {
            super(computeURI(className), Kind.SOURCE);
            this.code = code;
        }

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) {
            return code;
        }

        private static URI computeURI(String className) {
            return URI.create("string:///" + className.replace('.', '/') + Kind.SOURCE.extension);
        }
    }



}
