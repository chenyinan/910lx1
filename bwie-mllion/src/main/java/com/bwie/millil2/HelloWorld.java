package com.bwie.millil2;


import java.util.Map;

public class HelloWorld{
    public  String show(Map<String,Object> parm) {
        String originString = (String) parm.get("phone");
        Integer start = (Integer) parm.get("start");
        Integer end = (Integer) parm.get("end");
        String replace = (String) parm.get("replace");
        if (originString.length() == 11) {
            String prefix = originString.substring(0, start);
            String suffix = originString.substring(end);

            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(prefix);

            for (int i = 0; i < end - start; i++) {
                stringBuffer.append(replace);
            }
            stringBuffer.append(suffix);

            return stringBuffer.toString();
        }
        throw new RuntimeException("请传入11位手机号");
    }


}
