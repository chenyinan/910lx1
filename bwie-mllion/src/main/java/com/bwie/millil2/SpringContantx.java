package com.bwie.millil2;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SpringContantx  implements ApplicationContextAware {
    public static ApplicationContext applicationContext;

    public static void addBean(String name, Object bean) {
            //判断类型
            if (applicationContext instanceof ConfigurableApplicationContext) {
                //多态（父类对象的子类引用）
                ConfigurableApplicationContext configurableApplicationContext = (ConfigurableApplicationContext) applicationContext;
                //获取其中的容器工厂
                ConfigurableListableBeanFactory beanFactory = configurableApplicationContext.getBeanFactory();
                //在spring的容器工厂中注册一个单例的对象
                beanFactory.registerSingleton(name, bean); // 注册单例 bean
            }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContantx.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return SpringContantx.applicationContext;
    }

    public static Object getBean(String name) {
        return getApplicationContext().getBean(name);
    }

    public static <T>T getBean(Class clazz) {
        return (T)getApplicationContext().getBean(clazz);
    }

    public static <T>T getBean(String name, Class clazz) {
        return (T)getApplicationContext().getBean(name,clazz);
    }

    /**
     * 同步方法注册bean到ApplicationContext中
     *
     * @param beanName
     * @param clazz
     * @param original bean的属性值
     */



}
