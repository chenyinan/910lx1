package com.bwie.millil2;



import com.bwie.common.domain.request.Order;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserBatchHandler {

    public static int batchSave(List userList, String threadName)  {
        String insertSql ="INSERT INTO t_order1_copy1 (number,phone,create_time,upda_time) VALUES (?, ?, ?, ?)";
        //取得发送sql语句的对象
        PreparedStatement pst = null;

        Order order;

    /*    List<Order> list=  domes(userList);*/
        List<Order> list=  dome2(userList);

        int[] count = new int[0];

        Connection conn = null;

        try {

            conn= C3p0Util.getConnection();

            pst = conn.prepareStatement(insertSql);


            long start=System.currentTimeMillis();



            if(null!=list&&list.size()>0){
                for(int i=0;i<list.size();i++){

                    Date date = new Date();

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    String format = simpleDateFormat.format(date);

                    Order order1 = list.get(i);

                    pst.setString(1,order1.getNumber());
                    pst.setString(2,order1.getPhone());
                    pst.setString(3,format);
                    pst.setString(4,format);
                /*    user= (User) userList.get(i);
                    pst.setInt(1,user.getId());
                    pst.setString(2,user.getName());*/
                    //加入批处理
                    pst.addBatch();
                }

                count= pst.executeBatch();
                System.out.println(count.length);
                System.out.println(" threadName为"+threadName+", sync data to db, it  has spent " +(System.currentTimeMillis()-start)+"  ms");

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //4. 释放资源
            C3p0Util.close(conn, pst, null);
        }

        //获取到数据更新的行数
        return count.length;
    }

    private static List<Order> dome2(List<Order> list) {

        ArrayList<Order> list2 = new ArrayList<>();


        return list2;



    }

    private static List<Order> dome1(List<Order> list) {

        ArrayList<Order> list2 = new ArrayList<>();

            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();

            for (Order order : list) {
                kieSession.insert(order);
                list2.add(order);
            }

            kieSession.fireAllRules();

            kieSession.dispose();
        return list2;
    }



}

