package com.bwie.millin1;

import com.bwie.common.domain.request.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Producer {


    public static void main(String[] args) {
        Producer.createData();
    }



    public static void createData() {



        long starts = System.currentTimeMillis();



        ArrayList<Order> list = new ArrayList<>();


        Connection conn = C3p0Util.getConnection();

        Statement stmt = null;


        try {

            // 执行查询
            System.out.println(" 实例化Statement对象...");

            // 设置每页的大小和页码
            int pageSize = 1000000; // 每页的大小
            int pageNumber = 1; // 当前页码


            // 计算起始行和结束行
            int startRow = (pageNumber - 1) * pageSize;
            int endRow = pageNumber * pageSize;

            // 创建预编译语句
            String sql = "SELECT * FROM test_table LIMIT ?, ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            long end = System.currentTimeMillis();

            System.out.println("-----连接数据时间------------"+(end-starts));


            // 设置起始行和结束行参数
            preparedStatement.setInt(1, startRow);
            preparedStatement.setInt(2, pageSize);


            stmt = conn.createStatement();

            ResultSet resultSet = preparedStatement.executeQuery();

     /*       String sql;
            sql = "SELECT id, number,phone FROM test_table";*/
            /*  ResultSet rs = stmt.executeQuery(sql);*/

            // 展开结果集数据库
            while (resultSet.next()) {
                String name = resultSet.getString("number");
                String phone = resultSet.getString("phone");
                int age = resultSet.getInt("id");

                // 输出数据
                Order order = new Order();

                order.setPhone(phone);
                order.setCode(String.valueOf(age));
                order.setNumber(name);
                list.add(order);

            }
            // 完成后关闭
            resultSet.close();

            long end1 = System.currentTimeMillis();

            System.out.println("花费的时间为：" + (end1 - starts));//20000条：625 //1000000条:14733

            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }

            System.out.println(list);

        }

        long l = System.currentTimeMillis();

        System.out.println(l-starts);
        //创建固定线程池
        ExecutorService pool = Executors.newFixedThreadPool(100);

        final int totalPageNo = 50; //分50批次

        final int pageSize = 20000; //每页大小是2万条
        //共10w条数据，每页5000条数据，20个线程
        final long start = System.currentTimeMillis();
        //原子类
        final AtomicInteger atomicInt = new AtomicInteger();

        //集合数据总条数  除以  批次  =  每批插入的数据条数
        int size = list.size()/totalPageNo;

        for (int currentPageNo = 0; currentPageNo < totalPageNo; currentPageNo++) {
            final int finalCurrentPageNo = currentPageNo;
            Runnable run = new Runnable() {
                @Override
                public void run() {
                    List<Order> list1 = list.subList(finalCurrentPageNo*size, size*(finalCurrentPageNo+1));
                    atomicInt.addAndGet(UserBatchHandler.batchSave(list1, Thread.currentThread().getName()));
                    //入库的数据达到一百万条的时候就会有个统计.
                    if (atomicInt.get() == (totalPageNo * pageSize)) {
                        //如果有一百万的时候.就会在这里有个结果
                        System.out.println("同步数据到db，它已经花费 " + ((System.currentTimeMillis() - start) / 1000) + "  秒");
                    }
                }
            };
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            pool.execute(run);
        }

    }



}

