package com.bwie.millin1;

import com.bwie.common.domain.request.Order;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.sql.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Add1 {

    public static void main(String[] args) {


        long starts = System.currentTimeMillis();


        ArrayList<Order> list = new ArrayList<>();

        Connection conn = C3p0Util.getConnection();

        Statement stmt = null;


        try {

            // 执行查询
            System.out.println(" 实例化Statement对象...");


            // 设置每页的大小和页码
            int pageSize = 2000; // 每页的大小
            int pageNumber = 1; // 当前页码


            // 计算起始行和结束行
            int startRow = (pageNumber - 1) * pageSize;
            int endRow = pageNumber * pageSize;

            // 创建预编译语句
            String sql = "SELECT * FROM test_table LIMIT ?, ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            long end = System.currentTimeMillis();

            System.out.println("-----连接数据时间------------" + (end - starts));


            // 设置起始行和结束行参数
            preparedStatement.setInt(1, startRow);
            preparedStatement.setInt(2, pageSize);


            stmt = conn.createStatement();

            ResultSet resultSet = preparedStatement.executeQuery();

     /*       String sql;
            sql = "SELECT id, number,phone FROM test_table";*/
            /*  ResultSet rs = stmt.executeQuery(sql);*/

            // 展开结果集数据库
            while (resultSet.next()) {
                String name = resultSet.getString("number");
                String phone = resultSet.getString("phone");
                int age = resultSet.getInt("id");

                // 输出数据
                Order order = new Order();

                order.setPhone(phone);
                order.setCode(String.valueOf(age));
                order.setNumber(name);
                list.add(order);

            }
            // 完成后关闭
            resultSet.close();


            long end1 = System.currentTimeMillis();

            System.out.println("花费的时间为：" + (end1 - starts));//20000条：625 //1000000条:14733


            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }


            //------------------------------

            String a = "deop";
            // 创建KieServices实例
            KieServices kieServices = KieServices.Factory.get();
            // 创建KieFileSystem实例
            KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
            String s = "package rules;\n" +
                    "import com.bwie.common.domain.request.Order;\n" +
                    "import org.springframework.security.crypto.bcrypt.BCrypt;\n" +

                    "\n" +
                    "dialect  \"mvel\"\n" +
                    "\n" +
                    "rule \"rules_1\"\n" +
                    "    when\n" +
                    "        $author:Order($author.phone!=null)\n" +
                    "    then\n" +
                    "        String card = $author.getPhone();\n" +
                    "        String s = desensitizeCard(card);\n" +
                    "        $author." + "setPhone(s);\n" +
                    "end\n" +
                    "\n" +
                    "function String desensitizeCard(String card) {\n" +
                    "    // 脱敏逻辑，将身份证号码中的敏感信息替换为*\n" +
                    "    // 这里只是一个示例，实际的脱敏逻辑需要根据具体需求来实现\n" +
                    "    String s = BCrypt.hashpw(card, BCrypt.gensalt());\n" +
                    "    return s;\n" +
                    "}";

            // 将规则文件内容添加到KieFileSystem中
            kieFileSystem.write("src/main/resources/rules/" + a + ".drl", s);
            // 创建KieBuilder实例
            KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
            // 构建KieModule
            Results results = kieBuilder.buildAll().getResults();
            // 检查构建结果是否有错误
            if (results.hasMessages(Message.Level.ERROR)) {
                System.out.println(results.getMessages() + "-------");
                throw new IllegalStateException("规则文件编译失败！");
            }
            // 获取KieContainer
            KieContainer kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
            // 创建KieSession
            KieSession kieSession = kieContainer.newKieSession();
            // 创建Identity对象


//        author.setCard("411325200212062930");

            // 将Identity对象插入到KieSession中
            ExecutorService pool = Executors.newFixedThreadPool(10);


            ArrayList<Order> lists = new ArrayList<>();

            int size = list.size() / 10;


            /*for (int currentPageNo = 0; currentPageNo < 10; currentPageNo++) {
                final int finalCurrentPageNo = currentPageNo;
                Runnable run = new Runnable() {

                    @Override
                    public void run() {

                        List<Order> list1 = list.subList(finalCurrentPageNo*size, size*(finalCurrentPageNo+1));

                        for (Order order : list1) {
                            // 将Identity对象插入到KieSession中

                        }

                    }
                };
                     try {
                 Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
                pool.execute(run);
            }*/

    /*        kieSession.insert(order);
            // 执行规则
            kieSession.fireAllRules();
            lists.add(order);
            kieSession.dispose();*/
            // 执行规则

            System.out.println(lists);
        }

    }
}
