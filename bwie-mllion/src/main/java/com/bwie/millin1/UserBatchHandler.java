package com.bwie.millin1;



import com.bwie.common.domain.request.Order;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserBatchHandler {

    public static int batchSave(List userList, String threadName)  {
        String insertSql ="INSERT INTO t_order1_copy1 (number,phone,create_time,upda_time) VALUES (?, ?, ?, ?)";
        //取得发送sql语句的对象
        PreparedStatement pst = null;

        Order order;

    /*    List<Order> list=  domes(userList);*/
        List<Order> list=  dome2(userList);

        int[] count = new int[0];

        Connection conn = null;

        try {

            conn= C3p0Util.getConnectionLocal();

            pst = conn.prepareStatement(insertSql);


            long start=System.currentTimeMillis();



            if(null!=list&&list.size()>0){
                for(int i=0;i<list.size();i++){
                    Date date = new Date();

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    String format = simpleDateFormat.format(date);

                    Order order1 = list.get(i);

                    pst.setString(1,order1.getNumber());
                    pst.setString(2,order1.getPhone());
                    pst.setString(3,format);
                    pst.setString(4,format);
                /*    user= (User) userList.get(i);
                    pst.setInt(1,user.getId());
                    pst.setString(2,user.getName());*/
                    //加入批处理
                    pst.addBatch();
                }

                count= pst.executeBatch();
                System.out.println(count.length);
                System.out.println(" threadName为"+threadName+", sync data to db, it  has spent " +(System.currentTimeMillis()-start)+"  ms");

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //4. 释放资源
            C3p0Util.close(conn, pst, null);
        }

        //获取到数据更新的行数
        return count.length;
    }

    private static List<Order> dome2(List<Order> list) {

        ArrayList<Order> list2 = new ArrayList<>();

        String a = "deop";
        // 创建KieServices实例
        KieServices kieServices = KieServices.Factory.get();
        // 创建KieFileSystem实例
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();


        String s =
                "package rules;\n" +
                        "import com.bwie.common.domain.request.Order;\n" +
                        "import javax.crypto.Cipher;\n" +
                        "import javax.crypto.spec.SecretKeySpec;\n" +
                        "import java.util.Base64;\n" +
                        "\n" +
                        "dialect  \"mvel\"\n" +
                        "\n" +
                        "rule \"rules_1\"\n" +
                        "    when\n" +
                        "        $author:Order($author.number!=null)\n" +
                        "    then\n" +
                        "        String card = $author.getNumber();\n" +
                        "        String s = desensitizeCard(card);\n" +
                        "        $author.setNumber(s);\n" +
                        "end\n" +
                        "\n" +
                        "function String desensitizeCard(String card) {" +
                        "      String ALGORITHM = \"AES\";\n" +
                        "      String KEY = \"MySecretKey12345\";\n" +
                        "      SecretKeySpec keySpec = new SecretKeySpec(KEY.getBytes(), ALGORITHM);\n" +
                        "       Cipher cipher = Cipher.getInstance(ALGORITHM);\n" +
                        "        cipher.init(Cipher.ENCRYPT_MODE, keySpec);\n" +
                        "      byte[] encryptedBytes = cipher.doFinal(card.getBytes());\n" +
                        "    String s = Base64.getEncoder().encodeToString(encryptedBytes);\n" +
                        "    return s;\n" +
                        "}";


/*
                "package rules;\n" +
                "import com.bwie.common.domain.request.Order;\n" +
                "\n" +
                "dialect  \"mvel\"\n" +
                "\n" +
                "rule \"rules_1\"\n" +
                "    when\n" +
                "        $author:Order($author.number!=null)\n" +
                "    then\n" +
                "        String card = $author.getNumber();\n" +
                "        String s = desensitizeCard(card);\n" +
                "        $author.setNumber(s);\n" +
                "end\n" +
                "\n" +
                "function String desensitizeCard(String card) {\n" +
                "    // 脱敏逻辑，将身份证号码中的敏感信息替换为*\n" +
                "    // 这里只是一个示例，实际的脱敏逻辑需要根据具体需求来实现\n" +
                "    String s = card.substring(0,2)+\"****\"+card.substring(3);\n" +
                "    return s;\n" +
                "}";*/


//                "package rules;\n" +
//                "import com.bwie.common.domain.request.Order;\n" +
//                "import org.springframework.security.crypto.bcrypt.BCrypt;\n" +
//
//                "\n" +
//                "dialect  \"mvel\"\n" +
//                "\n" +
//                "rule \"rules_1\"\n" +
//                "    when\n" +
//                "        $author:Order($author.phone!=null)\n" +
//                "    then\n" +
//                "        String card = $author.getPhone();\n" +
//                "        String s = desensitizeCard(card);\n" +
//                "        $author." + "setPhone(s);\n" +
//                "end\n" +
//                "\n" +
//                "function String desensitizeCard(String card) {\n" +
//                "    // 脱敏逻辑，将身份证号码中的敏感信息替换为*\n" +
//                "    // 这里只是一个示例，实际的脱敏逻辑需要根据具体需求来实现\n" +
//                "    String s = BCrypt.hashpw(card, BCrypt.gensalt());\n" +
//                "    return s;\n" +
//                "}";

        // 将规则文件内容添加到KieFileSystem中


        kieFileSystem.write("src/main/resources/rules/" + a + ".drl", s);
        // 创建KieBuilder实例
        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
        // 构建KieModule
        Results results = kieBuilder.buildAll().getResults();
        // 检查构建结果是否有错误
        if (results.hasMessages(Message.Level.ERROR)) {
            System.out.println(results.getMessages() + "-------");
            throw new IllegalStateException("规则文件编译失败！");
        }
        // 获取KieContainer
        KieContainer kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
        // 创建KieSession
        KieSession kieSession = kieContainer.newKieSession();

        int i=0;
        for (Order order : list) {
            i++;
            kieSession.insert(order);
            //执行
            kieSession.fireAllRules();

            list2.add(order);
            System.out.println("Desensitized ID Number: " + order.getNumber()+"---------------------------"+i);
        }
        // 关闭KieSession
        kieSession.dispose();

        return list2;



    }

    private static List<Order> dome1(List<Order> list) {

        ArrayList<Order> list2 = new ArrayList<>();

            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();

            for (Order order : list) {
                kieSession.insert(order);
                list2.add(order);
            }

            kieSession.fireAllRules();

            kieSession.dispose();
        return list2;
    }



}

