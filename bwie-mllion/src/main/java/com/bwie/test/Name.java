package com.bwie.test;

import java.sql.*;

public class Name {

    public static void main(String[] args) {


        String url = "jdbc:mysql://localhost:3306";
        String username = "root";
        String password = "1234";
        Connection conn = null;

        Statement stmt = null;


        try {

            Class.forName("com.mysql.cj.jdbc.Driver");

            // 打开链接
            System.out.println("连接数据库...");
            conn = DriverManager.getConnection(url,username,password);
            // 执行查询
            System.out.println(" 实例化Statement对象...");



            // 创建预编译语句
            String sql = "SHOW DATABASES";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);


            stmt = conn.createStatement();

            ResultSet resultSet = preparedStatement.executeQuery();

     /*       String sql;
            sql = "SELECT id, number,phone FROM test_table";*/
            /*  ResultSet rs = stmt.executeQuery(sql);*/

            // 展开结果集数据库
            while (resultSet.next()) {
                String name = resultSet.getString("Database");
                System.out.println(name);

                // 输出数据

            }
            // 完成后关闭
            resultSet.close();



            long end1 = System.currentTimeMillis();




            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }


        }
    }
}
