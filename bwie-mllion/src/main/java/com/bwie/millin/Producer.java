package com.bwie.millin;

import com.bwie.common.domain.request.Order;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Producer {


    public static void main(String[] args) {
        Producer.createData();
    }

//    public static void main(String[] args) {
//
//
//        long starts = System.currentTimeMillis();
//        ArrayList<Order> list = new ArrayList<>();
//
//        Connection conn = C3p0Util.getConnection();
//
//        Statement stmt = null;
//
//
//
//        try {
//
//            // 执行查询
//            System.out.println(" 实例化Statement对象...");
//
//
//            // 设置每页的大小和页码
//            int pageSize = 100000; // 每页的大小
//            int pageNumber = 1; // 当前页码
//
//
//            // 计算起始行和结束行
//            int startRow = (pageNumber - 1) * pageSize;
//            int endRow = pageNumber * pageSize;
//
//            // 创建预编译语句
//            String sql = "SELECT * FROM test_table LIMIT ?, ?";
//
//            PreparedStatement preparedStatement = conn.prepareStatement(sql);
//
//            long end = System.currentTimeMillis();
//
//            System.out.println("-----连接数据时间------------"+(end-starts));
//
//
//            // 设置起始行和结束行参数
//            preparedStatement.setInt(1, startRow);
//            preparedStatement.setInt(2, pageSize);
//
//
//            stmt = conn.createStatement();
//
//            ResultSet resultSet = preparedStatement.executeQuery();
//
//     /*       String sql;
//            sql = "SELECT id, number,phone FROM test_table";*/
//            /*  ResultSet rs = stmt.executeQuery(sql);*/
//
//
//            // 展开结果集数据库
//            while (resultSet.next()) {
//                String name = resultSet.getString("number");
//                String phone = resultSet.getString("phone");
//                int age = resultSet.getInt("id");
//
//                // 输出数据
//                Order order = new Order();
//
//                order.setPhone(phone);
//                order.setCode(String.valueOf(age));
//                order.setNumber(name);
//                list.add(order);
//
//            }
//            // 完成后关闭
//            resultSet.close();
//
//
//
//            long end1 = System.currentTimeMillis();
//
//            System.out.println("花费的时间为：" + (end1 - starts));//20000条：625 //1000000条:14733
//
//
//            stmt.close();
//            conn.close();
//        } catch (SQLException se) {
//            // 处理 JDBC 错误
//            se.printStackTrace();
//        } catch (Exception e) {
//            // 处理 Class.forName 错误
//            e.printStackTrace();
//        } finally {
//            // 关闭资源
//            try {
//                if (stmt != null) {
//                    stmt.close();
//                }
//            } catch (SQLException se2) {
//            }// 什么都不做
//            try {
//                if (conn != null) {
//                    conn.close();
//                }
//            } catch (SQLException se) {
//                se.printStackTrace();
//            }
//
//            System.out.println(list);
//
//
//        }
//
//
//        int size = list.size() / 10;
//
//
//        ThreadPoolExecutor pool1 = new ThreadPoolExecutor(
//                12, 100,
//                60L, TimeUnit.SECONDS,
//                new LinkedBlockingQueue<>(1));
//
////1
//        pool1.execute(()->{
//            List<Order> list1 = list.subList(0, size);
//
//
//            Producer.createData( list1);
//        });
//
////2
//        pool1.execute(()->{
//            List<Order> list1 = list.subList(size, size*2);
//
//
//            Producer.createData(list1);
//        });
//
////3
//        pool1.execute(()->{
//            List<Order> list1 = list.subList(size*2, size*3);
//
//
//            Producer.createData( list1);
//        });
////3
//        pool1.execute(()->{
//            List<Order> list1 = list.subList(size*3, size*4);
//
////4
//            Producer.createData( list1);
//        });
////5
//        pool1.execute(()->{
//            List<Order> list1 = list.subList(size*4, size*5);
//
////6
//            Producer.createData( list1);
//        });
////7
//        pool1.execute(()->{
//            List<Order> list1 = list.subList(size*5, size*6);
//
//
//            Producer.createData(list1);
//        });
//
////8
//
//        pool1.execute(()->{
//            List<Order> list1 = list.subList(size*6, size*7);
//
//
//            Producer.createData( list1);
//        });
//
////9
//
//        pool1.execute(()->{
//            List<Order> list1 = list.subList(size*7, size*8);
//
//
//            Producer.createData(list1);
//        });
////10
//        pool1.execute(()->{
//            List<Order> list1 = list.subList(size*8, size*9);
//
//
//            Producer.createData( list1);
//        });
//
//
//        pool1.execute(()->{
//            List<Order> list1 = list.subList(size*9, size*10);
//
//
//            Producer.createData( list1);
//        });
//    }


    public static void createData(/*List<Order> list*/) {

        long starts = System.currentTimeMillis();



        ArrayList<Order> list = new ArrayList<>();

        Connection conn = C3p0Util.getConnection();

        Statement stmt = null;



        try {

            // 执行查询
            System.out.println(" 实例化Statement对象...");


            // 设置每页的大小和页码
            int pageSize = 100000; // 每页的大小
            int pageNumber = 1; // 当前页码


            // 计算起始行和结束行
            int startRow = (pageNumber - 1) * pageSize;
            int endRow = pageNumber * pageSize;

            // 创建预编译语句
            String sql = "SELECT * FROM test_table LIMIT ?, ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            long end = System.currentTimeMillis();

            System.out.println("-----连接数据时间------------"+(end-starts));


            // 设置起始行和结束行参数
            preparedStatement.setInt(1, startRow);
            preparedStatement.setInt(2, pageSize);


            stmt = conn.createStatement();

            ResultSet resultSet = preparedStatement.executeQuery();

     /*       String sql;
            sql = "SELECT id, number,phone FROM test_table";*/
            /*  ResultSet rs = stmt.executeQuery(sql);*/

            // 展开结果集数据库
            while (resultSet.next()) {
                String name = resultSet.getString("number");
                String phone = resultSet.getString("phone");
                int age = resultSet.getInt("id");

                // 输出数据
                Order order = new Order();

                order.setPhone(phone);
                order.setCode(String.valueOf(age));
                order.setNumber(name);
                list.add(order);

            }
            // 完成后关闭
            resultSet.close();



            long end1 = System.currentTimeMillis();

            System.out.println("花费的时间为：" + (end1 - starts));//20000条：625 //1000000条:14733


            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }

            System.out.println(list);


        }




        long l = System.currentTimeMillis();

        System.out.println(l-starts);
        ExecutorService pool = Executors.newFixedThreadPool(1010);

        final int totalPageNo = 500; //分50批次

        final int pageSize = 20000; //每页大小是2万条
        //共10w条数据，每页5000条数据，20个线程
        final long start = System.currentTimeMillis();

        final AtomicInteger atomicInt = new AtomicInteger();

        int size = list.size()/totalPageNo;

        for (int currentPageNo = 0; currentPageNo < totalPageNo; currentPageNo++) {
            final int finalCurrentPageNo = currentPageNo;
            Runnable run = new Runnable() {

                @Override
                public void run() {

                    List<Order> list1 = list.subList(finalCurrentPageNo*size, size*(finalCurrentPageNo+1));

                    atomicInt.addAndGet(UserBatchHandler.batchSave(list1, Thread.currentThread().getName()));
                    //入库的数据达到一百万条的时候就会有个统计.
                    if (atomicInt.get() == (totalPageNo * pageSize)) {
                        //如果有一百万的时候.就会在这里有个结果
                        System.out.println("同步数据到db，它已经花费 " + ((System.currentTimeMillis() - start) / 1000) + "  秒");
                    }
                }
            };
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            pool.execute(run);
        }

    }

    public static void createData1() {

        long starts = System.currentTimeMillis();
        ArrayList<Order> list = new ArrayList<>();

        Connection conn = C3p0Util.getConnection();

        Statement stmt = null;


        try {

            // 执行查询
            System.out.println(" 实例化Statement对象...");


            // 设置每页的大小和页码
            int pageSize = 100000; // 每页的大小
            int pageNumber = 100000; // 当前页码


            // 计算起始行和结束行
            int startRow = (pageNumber - 1) * pageSize;
            int endRow = pageNumber * pageSize;

            // 创建预编译语句
            String sql = "SELECT * FROM test_table LIMIT ?, ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            long end = System.currentTimeMillis();

            System.out.println("-----连接数据时间------------"+(end-starts));


            // 设置起始行和结束行参数
            preparedStatement.setInt(1, startRow);
            preparedStatement.setInt(2, pageSize);


            stmt = conn.createStatement();

            ResultSet resultSet = preparedStatement.executeQuery();

     /*       String sql;
            sql = "SELECT id, number,phone FROM test_table";*/
            /*  ResultSet rs = stmt.executeQuery(sql);*/


            // 展开结果集数据库
            while (resultSet.next()) {
                String name = resultSet.getString("number");
                String phone = resultSet.getString("phone");
                int age = resultSet.getInt("id");

                // 输出数据
                Order order = new Order();

                order.setPhone(phone);
                order.setCode(String.valueOf(age));
                order.setNumber(name);
                list.add(order);

            }
            // 完成后关闭
            resultSet.close();



            long end1 = System.currentTimeMillis();

            System.out.println("花费的时间为：" + (end1 - starts));//20000条：625 //1000000条:14733


            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }

            System.out.println(list);


        }





        long l = System.currentTimeMillis();

        System.out.println(l-starts);
        ExecutorService pool = Executors.newFixedThreadPool(100);

        final int totalPageNo = 50; //分50批次

        final int pageSize = 20000; //每页大小是2万条
        //共10w条数据，每页5000条数据，20个线程
        final long start = System.currentTimeMillis();

        final AtomicInteger atomicInt = new AtomicInteger();

        int size = list.size()/totalPageNo;

        for (int currentPageNo = 0; currentPageNo < totalPageNo; currentPageNo++) {
            final int finalCurrentPageNo = currentPageNo;
            Runnable run = new Runnable() {

                @Override
                public void run() {

                    List<Order> list1 = list.subList(finalCurrentPageNo*size, size*(finalCurrentPageNo+1));

                    atomicInt.addAndGet(UserBatchHandler.batchSave(list1, Thread.currentThread().getName()));
                    //入库的数据达到一百万条的时候就会有个统计.
                    if (atomicInt.get() == (totalPageNo * pageSize)) {
                        //如果有一百万的时候.就会在这里有个结果
                        System.out.println("同步数据到db，它已经花费 " + ((System.currentTimeMillis() - start) / 1000) + "  秒");
                    }
                }
            };
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            pool.execute(run);
        }

    }

}

