package com.bwie.millin;



import com.bwie.common.domain.request.Order;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class UserBatchHandler {

    public static int batchSave(List userList, String threadName)  {
        String insertSql ="INSERT INTO t_order1_copy1 (number,phone,create_time,upda_time) VALUES (?, ?, ?, ?)";
        //取得发送sql语句的对象
        PreparedStatement pst = null;

        Order order;

    /*    List<Order> list=  domes(userList);*/
        List<Order> list=  dome1(userList);
        int[] count = new int[0];

        Connection conn = null;
        try {


            conn= C3p0Util.getConnection();

            pst = conn.prepareStatement(insertSql);


            long start=System.currentTimeMillis();

            if(null!=list&&list.size()>0){
                for(int i=0;i<list.size();i++){

                    Date date = new Date();

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    String format = simpleDateFormat.format(date);

                    Order order1 = list.get(i);

                    pst.setString(1,order1.getNumber());

                    pst.setString(2,order1.getPhone());
                    pst.setString(3,format);
                    pst.setString(4,format);
                /*    user= (User) userList.get(i);
                    pst.setInt(1,user.getId());
                    pst.setString(2,user.getName());*/
                    //加入批处理
                    pst.addBatch();
                }

                count= pst.executeBatch();
                System.out.println(count.length);
                System.out.println(" threadName为"+threadName+", sync data to db, it  has spent " +(System.currentTimeMillis()-start)+"  ms");



            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //4. 释放资源
           /* C3p0Util.close(conn, pst, null);*/
        }

        //获取到数据更新的行数
        return count.length;
    }

    private static List<Order> domes(List userList) {

        ArrayList<Order> list = new ArrayList<>();

        String a = "deop";
        // 创建KieServices实例
        KieServices kieServices = KieServices.Factory.get();
        // 创建KieFileSystem实例
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        String s = "package rules;\n" +
                "import com.bwie.common.domain.request.Order;\n" +
                "\n" +
                "dialect  \"mvel\"\n" +
                "\n" +
                "rule \"rules_1\"\n" +
                "    when\n" +
                "        $author:Order($author.number!=null)\n" +
                "    then\n" +
                "        String card = $author.getNumber();\n" +
                "        String s = desensitizeCard(card);\n" +
                "        $author.setNumber(s);\n" +
                "end\n" +
                "\n" +
                "function String desensitizeCard(String card) {\n" +
                "    // 脱敏逻辑，将身份证号码中的敏感信息替换为*\n" +
                "    // 这里只是一个示例，实际的脱敏逻辑需要根据具体需求来实现\n" +
                "    String s = card.substring(0,2)+\"****\"+card.substring(6);\n" +
                "    return s;\n" +
                "}";

        // 将规则文件内容添加到KieFileSystem中
        kieFileSystem.write("src/main/resources/rules/"+a+".drl", s);
        // 创建KieBuilder实例
        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
        // 构建KieModule
        Results results = kieBuilder.buildAll().getResults();
        // 检查构建结果是否有错误
        if (results.hasMessages(Message.Level.ERROR)) {
            System.out.println(results.getMessages()+"-------");
            throw new IllegalStateException("规则文件编译失败！");
        }
        // 获取KieContainer
        KieContainer kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
        // 创建KieSession
        KieSession kieSession = kieContainer.newKieSession();
        // 创建Identity对象
        for (int i = 0; i <userList.size() ; i++) {
            Order order1 = (Order) userList.get(i);
            kieSession.insert(order1);     //对象添加到规则要
            list.add(order1);
        }



//        author.setCard("411325200212062930");

        // 将Identity对象插入到KieSession中

        // 执行规则
        kieSession.fireAllRules();
 /*       // 输出脱敏后的身份证号码
        System.out.println("Desensitized ID Number: " + order.getNumber());*/
        // 关闭KieSession
        kieSession.dispose();

        return null;
    }

    private static List<Order> dome1(List<Order> list) {

        ArrayList<Order> list2 = new ArrayList<>();



        ThreadPoolExecutor pool = new ThreadPoolExecutor(20,
                20,
                60L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(1));


        int size = list.size() / 10;


//1
        pool.execute(()->{

            List<Order> list1 = list.subList(0, size);

            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();
            for (Order order : list1) {
                kieSession.insert(order);
                list2.add(order);
            }


            kieSession.fireAllRules();

            kieSession.dispose();

        });
//2
        pool.execute(()->{

            List<Order> list1 = list.subList(size, size*2);


            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();
            for (Order order : list1) {
                kieSession.insert(order);
                list2.add(order);
            }

            kieSession.fireAllRules();

            kieSession.dispose();

        });
        //3
        pool.execute(()->{

            List<Order> list1 = list.subList(size*3, size*4);


            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();
            for (Order order : list1) {
                kieSession.insert(order);
                list2.add(order);
            }


            kieSession.fireAllRules();

            kieSession.dispose();
        });

        //4
        pool.execute(()->{

            List<Order> list1 = list.subList(size*4, size*5);


            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();
            for (Order order : list1) {
                kieSession.insert(order);
                list2.add(order);
            }

        });
        //5
        pool.execute(()->{

            List<Order> list1 = list.subList(size*5, size*6);


            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();
            for (Order order : list1) {
                kieSession.insert(order);
                list2.add(order);
            }

            kieSession.fireAllRules();

            kieSession.dispose();

        });
        //6
        pool.execute(()->{

            List<Order> list1 = list.subList(size*6, size*7);


            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();
            for (Order order : list1) {
                kieSession.insert(order);
                list2.add(order);
            }

            kieSession.fireAllRules();

            kieSession.dispose();
        });  //7
        pool.execute(()->{

            List<Order> list1 = list.subList(size*6, size*7);


            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();
            for (Order order : list1) {
                kieSession.insert(order);
                list2.add(order);
            }

            kieSession.fireAllRules();

            kieSession.dispose();
        });
//8
        pool.execute(()->{

            List<Order> list1 = list.subList(size*7, size*8);


            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();
            for (Order order : list1) {
                kieSession.insert(order);
                list2.add(order);
            }

            kieSession.fireAllRules();

            kieSession.dispose();

        });

        //9
        pool.execute(()->{

            List<Order> list1 = list.subList(size*7, size*8);


            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();

            for (Order order : list1) {
                kieSession.insert(order);
                list2.add(order);
            }

            kieSession.fireAllRules();

            kieSession.dispose();

        });
        //10
        pool.execute(()->{

            List<Order> list1 = list.subList(size*9, size*10);


            KieServices kieServices = KieServices.Factory.get();

            KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

            KieSession kieSession = kieClasspathContainer.newKieSession();
            for (Order order : list1) {
                kieSession.insert(order);
                list2.add(order);
            }


            kieSession.fireAllRules();

            kieSession.dispose();

        });

        return list2;
    }


    private static void deom(Order order) {

        long start = System.currentTimeMillis();


        KieServices kieServices = KieServices.Factory.get();

        KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

        KieSession kieSession = kieClasspathContainer.newKieSession();
        long end = System.currentTimeMillis();

        System.out.println( "----------初始化dome-------------"+(end-start) );

        kieSession.insert(order);//对象添加到规则要
        long end1 = System.currentTimeMillis();
        System.out.println( "----------初始化dome-------------"+(end1-start) );
        System.out.println();
        kieSession.fireAllRules();

        kieSession.dispose();
    }

}

