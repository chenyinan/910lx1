package com.bwie.mq.controller;

import com.bwie.common.result.Result;
import com.bwie.mq.config.DLXQueue;
import com.bwie.mq.config.TtlQueue;
import com.bwie.mq.consumer.DelayedQueue;
import com.bwie.mq.domain.User;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类说明
 *
 * @author zhuwenqiang
 * @date 2023/2/27
 */
@RestController
@RequestMapping("/mq")
public class RabbitMQController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private DelayedQueue delayedQueue;

    @Autowired
    private TtlQueue ttlQueue;

    @Autowired
    private DLXQueue dlxQueue;

    /**
     * hello world   一对一   一个生产者 一个消费者   不涉及到 交换机
     * @return
     */
    @GetMapping("/hello/world")
    public Result helloWorld() {
        // 通过 MQ 发送消息
        // 第一个参数 就是队列的名称  （String类型）
        // 第二个参数 消息内容  （Object 类型）
        // 定义队列的名称
        String queueName = "hello.word.queue";
        // 定义消息的内容
        String message = "hello word queue ~~";
        // 发送消息
        User user = new User();
        user.setUsername("张三");
        user.setAge(20);
        rabbitTemplate.convertAndSend(queueName, user);
        return Result.success("OK~~~");
    }

    @GetMapping("/work/queue")
    public Result workQueue() {
        // 发送消息 【10条】
        // 定义队列名称
        String queueName = "work.queue";
        String message = "work queue ~~~";
        for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend(queueName, message + i);
        }
        return Result.success("OK~~~");
    }

    @GetMapping("/fanout")
    public Result fanout() {
        // 通过 MQ 发送消息
        // 第一个参数 就是换机的名称  （String类型）
        // 第二个参数 队列名称 【“”】  （Object 类型）
        // 第三个参数 下次内容
        // 定义交换机的名称
        String exchangeName = "fanout.exchange";
        // 定义消息的内容
        String message = "hello word fanout ~~";
        // 发送消息
        rabbitTemplate.convertAndSend(exchangeName, "", message);
        return Result.success("OK~~~");
    }

    @GetMapping("/direct")
    public Result direct() {
        // 通过 MQ 发送消息
        // 第一个参数 就是换机的名称  （String类型）
        // 第二个参数 队列名称 【“”】  （Object 类型）
        // 第三个参数 下次内容
        // 定义交换机的名称
        String exchangeName = "direct.exchange";
        // 定义消息的内容
        String message = "hello word direct ~~";
        // 发送消息
        rabbitTemplate.convertAndSend(exchangeName, "info", message);
        return Result.success("OK~~~");
    }

    @GetMapping("/topic")
    public Result topic() {
        // 通过 MQ 发送消息
        // 第一个参数 就是换机的名称  （String类型）
        // 第二个参数 队列名称 【“”】  （Object 类型）
        // 第三个参数 下次内容
        // 定义交换机的名称
        String exchangeName = "topic.exchange";
        // 定义消息的内容
        String message = "hello word topic ~~";
        // 发送消息
        rabbitTemplate.convertAndSend(exchangeName, "age", message);
        return Result.success("OK~~~");
    }


    @GetMapping("/delay")
    public Result delay() {
        // 定义队列名称
        delayedQueue.sendDelayedQueue("delay.queue", "hello.delay~~", 5000);
        return Result.success("OK~~~");
    }

    @GetMapping("/ttl")
    public Result ttl() {
        // 定义队列名称
        ttlQueue.sendTtlQueue("ttl.queue", "hello.ttl~~", 5000);
        return Result.success("OK~~~");
    }

    @GetMapping("/dlx")
    public Result dlx() {
        // 定义队列名称
        dlxQueue.sendDLXQueue("dd.queue", "dlx.queue", "hello.dlx~~", 5000);
        return Result.success("OK~~~");
    }


}
