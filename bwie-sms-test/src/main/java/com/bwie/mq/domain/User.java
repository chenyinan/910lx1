package com.bwie.mq.domain;

import lombok.Data;

/**
 * 类说明
 *
 * @author zhuwenqiang
 * @date 2023/2/28
 */
@Data
public class User {

    /**
     * 用户名
     */
    private String username;

    /**
     * 年龄
     */
    private Integer age;

}
