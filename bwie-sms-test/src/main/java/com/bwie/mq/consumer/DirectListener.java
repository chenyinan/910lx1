package com.bwie.mq.consumer;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 类说明
 *
 * @author zhuwenqiang
 * @date 2023/2/28
 */
@Component
@Log4j2
public class DirectListener {

    @RabbitListener(bindings = { @QueueBinding(value = @Queue("test.queue"),
            exchange = @Exchange(name = "direct.exchange", type = ExchangeTypes.DIRECT),
        key = { "info", "error" })})
    public void directListener(String message) {
        log.info("消息消费者接收到消息，队列名称： test.queue， 消息的内容：【{}】，开始消费...", message);
    }

    @RabbitListener(bindings = { @QueueBinding(value = @Queue("test.queue2"),
            exchange = @Exchange(name = "direct.exchange", type = ExchangeTypes.DIRECT),
            key = { "info", "debug" })})
    public void directListener2(String message) {
        log.info("消息消费者接收到消息，队列名称： test.queue2， 消息的内容：【{}】，开始消费...", message);
    }

}
