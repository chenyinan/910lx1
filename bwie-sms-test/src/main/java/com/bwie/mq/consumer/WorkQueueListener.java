package com.bwie.mq.consumer;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 类说明
 *
 * @author zhuwenqiang
 * @date 2023/2/28
 */
@Component
@Log4j2
public class WorkQueueListener {

    /**
     * 消息队的消费者 [第一个]
     */
    @RabbitListener(queuesToDeclare = { @Queue("work.queue") })
    public void workQueueListener1(String message) throws InterruptedException {
        Thread.sleep(200);
        log.info("第一个消息消费者接收到消息，消息的内容：【{}】，开始消费...", message);
    }

    /**
     * 消息队的消费者 [第二个]
     */
    @RabbitListener(queuesToDeclare = { @Queue("work.queue") })
    public void workQueueListener2(String message) throws InterruptedException {
        Thread.sleep(400);
        log.info("第二个消息消费者接收到消息，消息的内容：【{}】，开始消费...", message);
    }

}
