package com.bwie.mq.consumer;

import com.bwie.mq.domain.User;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息的消费者
 *
 * @author zhuwenqiang
 * @date 2023/2/28
 */
@Configuration
@Log4j2
public class HelloWorldListener {

    /**
     * 初始化队列
     */
    @Bean
    public Queue initQueue() {
        // 第一个参数 队列的名称
        // 第二个参数 当前的队列是否支持 持久化
        log.info("初始化队列成功，队列名称：hello.word.queue");
        return new Queue("hello.word.queue", true);
    }

    /**
     * 监听队列消费消费  -> 业务
     */
    @RabbitListener(queues = "hello.word.queue")
    public void helloWorldListener(User message) {
        log.info("消息消费者接收到消息，消息的内容：【{}】，开始消费...", message);
        System.out.println("hello.word.queue消息是：" + message);
    }

}
