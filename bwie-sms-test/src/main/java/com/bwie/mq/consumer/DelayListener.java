package com.bwie.mq.consumer;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 类说明
 *
 * @author zhuwenqiang
 * @date 2023/3/1
 */
@Component
@Log4j2
public class DelayListener {

    @RabbitListener(queuesToDeclare = @Queue("delay.queue"))
    public void delayListener(String message) {
        log.info("消息消费者接收到消息，队列名称： delay.queue， 消息的内容：【{}】，开始消费...", message);
    }


}
