package com.bwie.mq.consumer;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 类说明
 *
 * @author zhuwenqiang
 * @date 2023/2/28
 */
@Component
@Log4j2
public class FanoutListener {

    /**
     * 消息的消费者  发布订阅模式  规律  点击去看
     * 定义队列
     * 定义交换机
     */
    @RabbitListener(bindings = { @QueueBinding( value = @Queue(value = "test.queue"),
            exchange = @Exchange(name = "fanout.exchange", type = ExchangeTypes.FANOUT))})
    public void fanoutListener(String message) {
        log.info("消息消费者接收到消息，队列名称： test.queue， 消息的内容：【{}】，开始消费...3", message);
    }

    /**
     * 消息的消费者  发布订阅模式  规律  点击去看
     * 定义队列
     * 定义交换机
     */
    @RabbitListener(bindings = { @QueueBinding( value = @Queue(value = "test.queue2"),
            exchange = @Exchange(name = "fanout.exchange", type = ExchangeTypes.FANOUT))})
    public void fanoutListener2(String message) {
        log.info("消息消费者接收到消息，队列名称： test.queue2， 消息的内容：【{}】，开始消费...2", message);
    }

    /**
     * 消息的消费者  发布订阅模式  规律  点击去看
     * 定义队列
     * 定义交换机
     */
    @RabbitListener(bindings = { @QueueBinding( value = @Queue(value = "test.queue2"),
            exchange = @Exchange(name = "fanout.exchange", type = ExchangeTypes.FANOUT))})
    public void fanoutListener3(String message) {
        log.info("消息消费者接收到消息，队列名称： test.queue2， 消息的内容：【{}】，开始消费...1", message);
    }


}
