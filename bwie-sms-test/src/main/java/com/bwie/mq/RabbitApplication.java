package com.bwie.mq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 消息测试启动类
 *
 * @author zhuwenqiang
 * @date 2023/2/28
 */
@SpringBootApplication
public class RabbitApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitApplication.class);
    }

}
