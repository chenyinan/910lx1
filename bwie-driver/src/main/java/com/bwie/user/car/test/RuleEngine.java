package com.bwie.user.car.test;



import com.bwie.user.car.rule.Rule;

import java.util.ArrayList;
import java.util.List;

//
public class RuleEngine {
    private List<Rule> rules;//一个规则列表（List<Rule>）来存储规则

    public RuleEngine() {
        rules = new ArrayList<>();
    }

    public void addRule(Rule rule) {//addRule()方法添加规则
        rules.add(rule);
    }

    public void evaluateRules(Vehicle vehicle) {//evaluateRules()方法对给定的车辆对象进行规则评估和执行
        for (Rule rule : rules) {
            if (rule.evaluate(vehicle)) {
                rule.execute(vehicle);
            }
        }
    }


}
