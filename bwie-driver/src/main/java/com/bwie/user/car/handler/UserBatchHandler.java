package com.bwie.user.car.handler;



import com.bwie.common.domain.request.Order;
import com.bwie.user.car.test.Vehicle;
import com.bwie.user.car.util.C3p0Util;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserBatchHandler {

    public static int batchSave(List userList, String threadName)  {

        String insertSql ="INSERT INTO t_type (type,type_name,create_time) VALUES (?, ?, ?)";
        //取得发送sql语句的对象
        PreparedStatement pst = null;

        int[] count = new int[0];

        Connection conn = null;

        try {

            //建立连接
            conn= C3p0Util.getConnectionLocal();


            pst = conn.prepareStatement(insertSql);
            // 创建SQL语句和PreparedStatement对象

            long start=System.currentTimeMillis();

            if(null!=userList&&userList.size()>0){
                for(int i=0;i<userList.size();i++){
                    Date date = new Date();

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    String format = simpleDateFormat.format(date);
                    Vehicle vehicle = (Vehicle) userList.get(i);

                    pst.setString(1,vehicle.getName());
                    pst.setString(2,vehicle.getType());
                    pst.setString(3,format);

                    //加入批处理
                    pst.addBatch();
                }

                count= pst.executeBatch();
                System.out.println(count.length);
                System.out.println(" threadName为"+threadName+", sync data to db, it  has spent " +(System.currentTimeMillis()-start)+"  ms");

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //4. 释放资源
            C3p0Util.close(conn, pst, null);
        }

        //获取到数据更新的行数
        return count.length;
    }




}

