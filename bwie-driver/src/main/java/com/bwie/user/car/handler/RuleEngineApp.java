package com.bwie.user.car.handler;


import com.bwie.common.domain.request.Order;
import com.bwie.user.car.test.RuleEngine;
import com.bwie.user.car.test.TrafficViolationRule;
import com.bwie.user.car.test.Vehicle;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class RuleEngineApp {


    public static void main(String[] args) {

        ArrayList<Vehicle> vehicles = new ArrayList<>();

        // 创建规则引擎
        RuleEngine ruleEngine = new RuleEngine();

        // 添加交通违章规则
        ruleEngine.addRule(new TrafficViolationRule());

//        // 创建车辆对象
//        Vehicle vehicle = new Vehicle(true,true,"320411200208116718");
//        Vehicle vehicle1 = new Vehicle(false,false,"357411201480119818");
//        Vehicle vehicle2 = new Vehicle(true,false,"3459645952656");
//        Vehicle vehicle3 = new Vehicle(false,true,"941656595656565557");
//
//        // 评估规则并生成处罚决定
//        ruleEngine.evaluateRules(vehicle);
//        ruleEngine.evaluateRules(vehicle1);
//        ruleEngine.evaluateRules(vehicle2);
//        ruleEngine.evaluateRules(vehicle3);


        System.out.println("all right");

        for (int i = 0; i <100000; i++) {
            Vehicle vehicle = new Vehicle(true,true,"320411200208116718");
            Vehicle vehicle1 = new Vehicle(false,false,"357411201480119818");
            Vehicle vehicle2 = new Vehicle(true,false,"3459645952656");
            Vehicle vehicle3 = new Vehicle(false,true,"941656595656565557");

            // 评估规则并生成处罚决定
            ruleEngine.evaluateRules(vehicle);
            ruleEngine.evaluateRules(vehicle1);
            ruleEngine.evaluateRules(vehicle2);
            ruleEngine.evaluateRules(vehicle3);

            if (vehicle1.getType()!=null){
                vehicles.add(vehicle1);
            }
            if (vehicle.getType()!=null){
                vehicles.add(vehicle);
            }
            if (vehicle2.getType()!=null){
                vehicles.add(vehicle2);
            }
            if (vehicle3.getType()!=null){
                vehicles.add(vehicle3);
            }

        }

        //创建固定线程池
        ExecutorService pool = Executors.newFixedThreadPool(100);

        final int totalPageNo = 50; //分50批次

        final int pageSize = 20000; //每页大小是2万条
        //共10w条数据，每页5000条数据，20个线程
        final long start = System.currentTimeMillis();
        //原子类
        final AtomicInteger atomicInt = new AtomicInteger();

        //集合数据总条数  除以  批次  =  每批插入的数据条数
        int size = vehicles.size()/totalPageNo;
        int size1 = vehicles.size()%totalPageNo;

        for (int currentPageNo = 0; currentPageNo < totalPageNo; currentPageNo++) {
            final int finalCurrentPageNo = currentPageNo;
            Runnable run = new Runnable() {
                @Override
                public void run() {
                    List<Vehicle> vehicles1 = vehicles.subList(finalCurrentPageNo * size, size * (finalCurrentPageNo + 1));

                    if (vehicles.size()%totalPageNo!=0&&finalCurrentPageNo==totalPageNo-1){
                        List<Vehicle> vehicles2 = vehicles.subList(size * (finalCurrentPageNo + 1), vehicles.size());
                        atomicInt.addAndGet(UserBatchHandler.batchSave(vehicles2, Thread.currentThread().getName()));
                        System.out.println("------------6-------6--6----------");
                    }

                    atomicInt.addAndGet(UserBatchHandler.batchSave(vehicles1, Thread.currentThread().getName()));
                    //入库的数据达到一百万条的时候就会有个统计.
                    if (atomicInt.get() == (totalPageNo * pageSize)) {
                        //如果有一百万的时候.就会在这里有个结果
                        System.out.println("同步数据到db，它已经花费 " + ((System.currentTimeMillis() - start) / 1000) + "  秒");
                    }
                }
            };
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            pool.execute(run);
        }

        System.out.println("all Right");
    }

}
