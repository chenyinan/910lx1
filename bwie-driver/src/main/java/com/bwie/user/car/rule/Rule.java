package com.bwie.user.car.rule;

import com.bwie.user.car.test.Vehicle;

//规则执行接口
public interface Rule {
    boolean evaluate(Vehicle vehicle);//判断方法
    void execute(Vehicle vehicle);//执行方法
}
