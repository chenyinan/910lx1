package com.bwie.user.car.test;



import com.alibaba.fastjson.JSON;
import com.bwie.user.car.rule.Rule;
import com.bwie.user.car.util.C3p0Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TrafficViolationRule implements Rule {



    @Override
    public boolean evaluate(Vehicle vehicle) {
        return vehicle.isRunningRedLight() || vehicle.isSpeeding();
    }

    @Override
    public void execute(Vehicle vehicle) {


        if (vehicle.isRunningRedLight()&&!vehicle.isSpeeding() ){
            System.out.println("A----------------");
            vehicle.setType("A");
        }


        if (vehicle.isSpeeding()&&!vehicle.isRunningRedLight()){
            System.out.println("-------B");
            vehicle.setType("B");
        }

        if (vehicle.isSpeeding()&&vehicle.isRunningRedLight()){
            System.out.println("C");
            vehicle.setType("--------C----------");
        }
        System.out.println("Now------------------------------------>"+vehicle);





//        redisTemplate.opsForSet().add("numList", JSON.toJSONString(vehicle));


//
//        String insertSql ="INSERT INTO t_type (type,type_name,create_time) VALUES (?, ?, ?)";
//        //取得发送sql语句的对象
//        PreparedStatement pst = null;
//
//
//        Connection conn = null;
//
//        try {
//
//            conn= C3p0Util.getConnectionLocal();
//
//            pst = conn.prepareStatement(insertSql);
//
//            long start=System.currentTimeMillis();
//
//
//            Date date = new Date();
//
//
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//                    String format = simpleDateFormat.format(date);
//
//
//
//                    pst.setString(1,vehicle.getType());
//                    pst.setString(2,vehicle.getName());
//                    pst.setString(3,format);
//
//                    pst.executeUpdate();
//
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }finally {
//            //4. 释放资源
//            C3p0Util.close(conn, pst, null);
//        }
    }
}
