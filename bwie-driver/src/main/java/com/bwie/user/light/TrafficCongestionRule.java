package com.bwie.user.light;

public class TrafficCongestionRule implements Rule{
    @Override
    public boolean evaluate(TrafficData trafficData) {
        return trafficData.getTrafficSpeed() < 30; // 假设速度低于30表示拥堵
    }

    @Override
    public void execute(TrafficData trafficData) {

        System.out.println("Traffic congestion detected: " + trafficData);
        // TODO: Take appropriate action, e.g., notify authorities
    }
}
