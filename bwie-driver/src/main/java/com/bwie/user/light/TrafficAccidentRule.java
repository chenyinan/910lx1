package com.bwie.user.light;

class TrafficAccidentRule implements Rule {
    @Override
    public boolean evaluate(TrafficData trafficData) {
        return trafficData.isAccident(); // 假设是否发生事故
    }

    @Override
    public void execute(TrafficData trafficData) {
        System.out.println("Traffic accident detected: " + trafficData);
        // TODO: Take appropriate action, e.g., notify emergency services
    }
}
