package com.bwie.user.light;

public class TrafficData {
    private int trafficSpeed;
    private boolean accident;

    public TrafficData(int trafficSpeed, boolean accident) {
        this.trafficSpeed = trafficSpeed;
        this.accident = accident;
    }


    public int getTrafficSpeed() {
        return trafficSpeed;
    }

    public boolean isAccident() {
        return accident;
    }

    @Override
    public String toString() {
        return "TrafficData{" +
                "trafficSpeed=" + trafficSpeed +
                ", accident=" + accident +
                '}';
    }
}

