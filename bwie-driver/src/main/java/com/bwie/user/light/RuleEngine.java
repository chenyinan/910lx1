package com.bwie.user.light;


import java.util.ArrayList;
import java.util.List;

//规则引擎类
class RuleEngine {

    //RuleEngine 类负责管理规则并执行评估和执行操作的过程
    private List<Rule> rules;
    //接口对象

    public RuleEngine() {
        rules = new ArrayList<>();
    }

    public void addRule(Rule rule) {//接收规则
        rules.add(rule);
    }

    public void evaluateRules(TrafficData trafficData) {
        for (Rule rule : rules) {
            if (rule.evaluate(trafficData)) {
                rule.execute(trafficData);
            }
        }
    }
}
