package com.bwie.user.light;


//Rule接口 判断是否拥堵
public interface Rule {
    //evaluate
    boolean evaluate(TrafficData trafficData);

    void execute(TrafficData trafficData);
}
