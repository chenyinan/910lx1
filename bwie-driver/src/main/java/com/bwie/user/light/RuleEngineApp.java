package com.bwie.user.light;

public class RuleEngineApp {
    public static void main(String[] args) {
        // 创建规则引擎
        RuleEngine ruleEngine = new RuleEngine();

        // 添加交通拥堵规则
        ruleEngine.addRule(new TrafficCongestionRule());

        // 添加交通事故规则
        ruleEngine.addRule(new TrafficAccidentRule());


            TrafficData trafficData1 = new TrafficData(25, false);
            ruleEngine.evaluateRules(trafficData1);

        // 模拟实时交通数据
      // 速度低于30，拥堵
        TrafficData trafficData2 = new TrafficData(20, true); // 事故
        TrafficData trafficData3 = new TrafficData(40, true); // 事故
        TrafficData trafficData4 = new TrafficData(40, false); // ，拥堵

        // 评估规则并采取相应措施
        ruleEngine.evaluateRules(trafficData2);
        ruleEngine.evaluateRules(trafficData3);
        ruleEngine.evaluateRules(trafficData4);
    }
}
