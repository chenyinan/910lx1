package com.bwie.user.lights.run;

import lombok.Data;

@Data
public class RuleType {
    private int carMany;
    private int carTime;
    private String carType;
}
