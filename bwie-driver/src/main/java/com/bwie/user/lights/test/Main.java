package com.bwie.user.lights.test;

import com.bwie.user.lights.color.TrafficSignal;
import com.bwie.user.lights.run.Rule;
import com.bwie.user.lights.run.RuleType;

public class Main {
    public static void main(String[] args) {

        //创建规则引擎数据

        RuleType ruleType = new RuleType();

        ruleType.setCarMany(500);

        ruleType.setCarTime(80);

        ruleType.setCarType("半拥堵");


        // 创建规则引擎
        RuleEngine ruleEngine = new RuleEngine();

        // 创建交通信号灯控制规则
        Rule trafficSignalRule = new TrafficSignalRule();

        // 添加交通信号灯控制规则到规则引擎
        ruleEngine.addRule(trafficSignalRule);

        // 创建交通信号灯控制规则的上下文对象
        TrafficSignalRuleContext context = new TrafficSignalRuleContext(1500, 90, "半拥堵", TrafficSignal.GREEN,ruleType);

        // 执行规则引擎
        ruleEngine.execute(context);

        // 输出交通信号灯的状态
        System.out.println("交通信号灯的状态为：" + context.getTrafficSignal());

    }
}
