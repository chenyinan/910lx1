package com.bwie.user.lights.test;

import com.bwie.user.lights.run.Rule;
import com.bwie.user.lights.run.RuleContext;

import java.util.ArrayList;
import java.util.List;
//规则引擎
public class RuleEngine {
    private List<Rule> rules;

    public RuleEngine() {
        rules = new ArrayList<>();
    }

    // 添加规则
    public void addRule(Rule rule) {
        rules.add(rule);
    }

    // 执行规则引擎
    public void execute(RuleContext context) {
        for (Rule rule : rules) {
            if (rule.evaluate(context)) {
                rule.execute(context);
            }
        }
    }
}
