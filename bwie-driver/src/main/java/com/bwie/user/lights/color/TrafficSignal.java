package com.bwie.user.lights.color;

public enum TrafficSignal {
    RED,
    YELLOW,
    GREEN
}
