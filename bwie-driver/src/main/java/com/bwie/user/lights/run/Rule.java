package com.bwie.user.lights.run;

//规则接口
public interface Rule {
    // 条件判断方法
    boolean evaluate(RuleContext context);

    // 规则操作方法
    void execute(RuleContext context);
}
