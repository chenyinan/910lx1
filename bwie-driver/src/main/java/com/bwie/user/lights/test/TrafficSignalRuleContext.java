package com.bwie.user.lights.test;

import com.bwie.user.lights.color.TrafficSignal;
import com.bwie.user.lights.run.Rule;
import com.bwie.user.lights.run.RuleContext;
import com.bwie.user.lights.run.RuleType;
import lombok.Data;

//规则上下文上下文
@Data
public class TrafficSignalRuleContext implements RuleContext {
    private int trafficFlow;
    private int time;//时间
    private String roadCondition;
    private TrafficSignal trafficSignal;//红绿灯
    private RuleType ruleType;

    //自定义的类

    public TrafficSignalRuleContext(int trafficFlow, int time, String roadCondition, TrafficSignal trafficSignal, RuleType ruleType) {
        this.trafficFlow = trafficFlow;
        this.time = time;
        this.roadCondition = roadCondition;
        this.trafficSignal = trafficSignal;
        this.ruleType = ruleType;
    }


    public TrafficSignalRuleContext() {
    }
}
