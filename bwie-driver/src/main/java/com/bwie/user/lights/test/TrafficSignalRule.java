package com.bwie.user.lights.test;


import com.bwie.user.lights.color.TrafficSignal;
import com.bwie.user.lights.run.Rule;
import com.bwie.user.lights.run.RuleContext;
import com.bwie.user.lights.run.RuleType;

public class TrafficSignalRule implements Rule {
    @Override
    public boolean evaluate(RuleContext context) {

        TrafficSignalRuleContext trafficSignalContext = (TrafficSignalRuleContext) context;//RuleContext转TrafficSignalRuleContext

        int trafficFlow = trafficSignalContext.getTrafficFlow();//当前车流量
        int time = trafficSignalContext.getTime();//时间
        String roadCondition = trafficSignalContext.getRoadCondition();//路况
        TrafficSignal trafficSignal = trafficSignalContext.getTrafficSignal();//红绿灯状况
        RuleType ruleType = ((TrafficSignalRuleContext) context).getRuleType();



        // 根据交通流量、时间、路况等条件来判断是否需要改变交通信号灯的状态
        // 返回true表示满足条件，需要执行规则的操作
        // 返回false表示不满足条件，不执行规则的操作
        return trafficFlow >
                ruleType.getCarMany() &&
                time > ruleType.getCarTime() && roadCondition.equals(ruleType.getCarType())
                &&trafficSignal.equals(TrafficSignal.RED);
    }

    @Override
    public void execute(RuleContext context) {

        TrafficSignalRuleContext trafficSignalContext = (TrafficSignalRuleContext) context;

        // 根据交通流量、时间、路况等条件来改变交通信号灯的状态
        trafficSignalContext.setTrafficSignal(TrafficSignal.YELLOW);


        Thread redThread = new Thread(() -> {
            try {
                    Thread.sleep(10000); // 红灯持续时间为10秒
                    // 根据交通流量、时间、路况等条件来改变交通信号灯的状态
                    trafficSignalContext.setTrafficSignal(TrafficSignal.GREEN);

                    System.out.println(trafficSignalContext.getTrafficSignal()+"-----------------------------------");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        redThread.start();




    }
}
