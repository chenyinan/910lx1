package com.bwie.user.lights.run;

import com.bwie.user.lights.color.TrafficSignal;
//上下文接口
public interface RuleContext {
    //车流量
    int getTrafficFlow();

    // 获取时间
    int getTime();

    // 获取路况
    String getRoadCondition();

    // 设置交通信号灯状态
    void setTrafficSignal(TrafficSignal signal);
}
