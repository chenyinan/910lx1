package com.bwie.light.car.test;

import lombok.Data;

@Data
public class Datas {

    private String name;
    private String type;
}
