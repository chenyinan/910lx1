package com.bwie.light.car.controller;

import com.bwie.common.result.Result;
import com.bwie.light.car.test.Vehicle;
import com.bwie.light.car.test.RuleEngine;
import com.bwie.light.car.test.TrafficViolationRule;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class RuleController {


    @PostMapping("/list")
    public Result list(){
                // 创建规则引擎
        RuleEngine ruleEngine = new RuleEngine();

        // 添加交通违章规则
        ruleEngine.addRule(new TrafficViolationRule());

        // 创建车辆对象
        for (int i = 0; i < 10000; i++) {
            Vehicle vehicle = new Vehicle(true,true,"320411200208116718");
            Vehicle vehicle1 = new Vehicle(false,false,"357411201480119818");
            Vehicle vehicle2 = new Vehicle(true,false,"3459645952656");
            Vehicle vehicle3 = new Vehicle(false,true,"941656595656565557");
            // 评估规则并生成处罚决定
            ruleEngine.evaluateRules(vehicle);
            ruleEngine.evaluateRules(vehicle1);
            ruleEngine.evaluateRules(vehicle2);
            ruleEngine.evaluateRules(vehicle3);
        }
        System.out.println("all Right");
        return Result.success("成功");
    }
}
