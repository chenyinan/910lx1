package com.bwie.light.car.handler;


import com.alibaba.fastjson.JSON;
import com.bwie.light.car.test.RuleEngine;
import com.bwie.light.car.test.TrafficViolationRule;
import com.bwie.light.car.test.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
public class RuleEngineApp {


    public static void main(String[] args) {

        SpringApplication.run(RuleEngineApp.class);
    }
}
