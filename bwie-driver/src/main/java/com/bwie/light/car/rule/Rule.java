package com.bwie.light.car.rule;

import com.bwie.light.car.test.Vehicle;

//规则接口
public interface Rule {
    boolean evaluate(Vehicle vehicle);

    void execute(Vehicle vehicle);
}
