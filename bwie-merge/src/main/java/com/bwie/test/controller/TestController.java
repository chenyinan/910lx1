package com.bwie.test.controller;

import com.bwie.common.result.Result;
import com.bwie.test.service.ShopService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
public class TestController {

    @Autowired
    private ShopService shopService;


    @PostMapping("/find")
    public Result result(){
        return shopService.findId();
    }
}
