package com.bwie.gateway.filters;

import com.bwie.common.constants.JwtConstants;
import com.bwie.common.utils.JwtUtils;
import com.bwie.common.utils.StringUtils;
import com.bwie.gateway.config.IgnoreWhiteConfig;
import com.bwie.gateway.utils.GatewayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class Fifter implements Ordered, GlobalFilter {

    @Autowired
    private IgnoreWhiteConfig ignoreWhiteConfig;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        List<String> whites = ignoreWhiteConfig.getWhites();

        ServerHttpRequest request = exchange.getRequest();
        String path = request.getURI().getPath();

        if (StringUtils.matches(path,whites)){
            return chain.filter(exchange);
        }

        String token = request.getHeaders().getFirst("token");
        if (token==null||token==""){
            return GatewayUtils.errorResponse(exchange,"token不能为空");
        }


        try {
            JwtUtils.parseToken(token);

        } catch (Exception exception) {
            exception.printStackTrace();
            return GatewayUtils.errorResponse(exchange,"token异常");
        }

        String userKey = JwtUtils.getUserKey(token);
        Boolean aBoolean = redisTemplate.hasKey(JwtConstants.USER_KEY + userKey);

        if (!aBoolean){
            return GatewayUtils.errorResponse(exchange,"token过期");
        }


        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
